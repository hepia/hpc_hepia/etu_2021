#!/bin/bash

#SBATCH --nodes=1
#SBATCH --ntasks-per-node=2
#SBATCH --cpus-per-task=10
#SBATCH --mem=0
#SBATCH --output=slurm-%J.out
#SBATCH --time=01:00:00
#SBATCH --partition=shared-cpu

module purge
module load GCC/10.2.0 CUDA/11.1.1 OpenMPI/4.0.5 Spark/3.1.1

# This section will be run when started by sbatch
if [ "$1" != 'srunning' ]; then
    export SPARK_WORKER_DIR=$HOME/spark-work-dir/
    export SPARK_LOCAL_DIRS=$HOME/spark-work-dir/
    export SPARK_CONF_DIR=$HOME/conf/
    export SPARK_CLUSTER_DIR=/home/users/k/kuenzlip/GIT/spark-hpc
    srun sh $SPARK_CLUSTER_DIR/launch_spark_cluster.sh srunning
# If run by srun, then decide by $SLURM_PROCID whether we are master or worker
else
    if [ "$SLURM_PROCID" -eq 0 ]; then
        export SPARK_MASTER_IP=$( hostname )
        MASTER_NODE=$( scontrol show hostname $SLURM_NODELIST | head -n 1 ):7077

        "spark-class" org.apache.spark.deploy.master.Master \
            --ip "$SPARK_MASTER_IP" \
            --port 7077 \
            --properties-file $SPARK_CONF_DIR/spark-defaults.conf 
    else
        # $(scontrol show hostname) is used to convert e.g. host20[39-40]
        # to host2039 this step assumes that SLURM_PROCID=0 corresponds to 
        # the first node in SLURM_NODELIST !
        MASTER_NODE=spark://$( scontrol show hostname $SLURM_NODELIST | head -n 1 ):7077
        "spark-class" org.apache.spark.deploy.worker.Worker $MASTER_NODE \
        --properties-file $SPARK_CONF_DIR/spark-defaults.conf 
    fi
fi
