jobid=$( sbatch launch_spark_cluster.sh )
jobid=${jobid##Submitted batch job }
master=''
filename="slurm-$jobid.out"

# wait for the slurm job to start and get spark master
while [ -z "$master" ]; do 
    sleep 1s
    if [ -f $filename ]; then
        master=$( sed -n -r 's|.*Starting Spark master at (spark://[0-9.:]*)|\1|p' $filename )
    fi
done

# wait a bit for all workers to register
sleep 5s

# run the spark program
echo $master
spark-submit --master $master sparkpi.py 32

# terminate slurm job
scancel $jobid
