# Corrections de la série

**Afin que la pédagogie fonctionne, nous vous conseillons de jeter un oeil à la correction uniquement si vous avez essayé d’accomplir la série par vous-même.**

## À propos de MPI_Send et MPI_Recv

**Question:** sont-elles bloquantes ? 

**Réponse:** elles le sont.

**Question:** utilisent-elles une mémoire tampon ? 

## Exercice 1

Cet exercice à été discuté lors de la séance d'exercices.

## Exercice 2

Utiliser un `MPI_Isend` en place du `MPI_Ssend` associé à un `MPI_Wait` avant que le buffer d'envoi soit écrasé:
```
bufSend[0] = bufRecv[0];
```

## Exercice 3

```c
#include <mpi.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int const MAX_ARY_SIZE = 10;

// Affiche un tableau (vecteur) d'entiers
void print_ary(int* ary, int n) {  
  for (int i = 0; i < n; i++) {
    printf("%d ", ary[i]);
  }    
  printf("\n");
}

// Initialise le générateur de nombre aléatoire avec
// une graine différente pour chaque noeud de calcul
void initRndGen(int rank) {
  srand(time(NULL) + rank);
}

// Génère un entier entre 1 et maxInt
int getInt(int maxInt) {
  int n = rand() % maxInt + 1;
  return n;
}

// Génère un tableau d'entiers dont la taille est aléatoire
// la taille "n" peut varier de 1 à 10 et les éléments du tableau
// vont de 1 à n
int* generateAry(int n) {  
  int* ary = malloc(n*sizeof(int));
  for ( int i = 0; i < n; i++ ) {
    ary[i] = i + 1;
  }
  return ary;
}

// temps de sommeil aléatoire entre 1 et 10 secondes
void napTime() {
  int sleepTime = rand() % 10 + 1;    
  sleep( sleepTime );
}

int main(int argc, char **argv) {
  int const TAG_SIZE = 100;
  int const TAG_ARY = 200;
  
  int myRank; 
  int nProc;
  
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  MPI_Comm_size(MPI_COMM_WORLD, &nProc);  
  
  initRndGen(myRank);

  if ( myRank == 0 ) {
    int k;
    int aryDone = 0;
    
    int temp_buff;
    int* recv_buff = (int*) malloc(MAX_ARY_SIZE*sizeof(int));
    
    int* ary_sizes = (int*) malloc((nProc - 1)*sizeof(int));
    int** results = (int**) malloc((nProc - 1)*sizeof(int*));    
    
    
    while (aryDone < (nProc - 1)) {
      // Réception de la taille et allocation
      MPI_Recv(&temp_buff, 1, MPI_INT, MPI_ANY_SOURCE, TAG_SIZE, 
               MPI_COMM_WORLD, &status);
      
      k = status.MPI_SOURCE;
      
      ary_sizes[k] = temp_buff;
      other_arys[k] = (int*) malloc(ary_sizes[k]*sizeof(int));
      
      // Réception du tableau
      MPI_Recv(&recv_buff, MAX_ARY_SIZE, MPI_INT, MPI_ANY_SOURCE, TAG_ARY,
               MPI_COMM_WORLD, &status);
      
      k = status.MPI_SOURCE;
      
      for (int i = 0; i < ary_sizes[k]; i++) {
      	other_arys[k][i] = recv_buff[i];
      }
    }
    
    // Affichage
    for (int i = 0; i < (nProc - 1); i++) {
      printf("Array: %d\n", i);
      print_ary(other_arys[i], ary_sizes[i]);
	}
  }
  else {
    // Création de la donnée
	int n_elem = getInt(MAX_ARY_SIZE);
	int* ary = generateAry(n_elem);
		
    napTime();
    // Envoi de la taille ici
    MPI_Send(&n_elem, 1, MPI_INT, 0, TAG_SIZE, MPI_COMM_WORLD);
    
    napTime();
    // Envoi de la donnée ici
    MPI_Send(&ary, n_elem, MPI_INT, 0, TAG_ARY, MPI_COMM_WORLD);
  }
  
  MPI_Finalize();
}
```