# Mesure de performance d'un code parallèle

[[_TOC_]]

## Introduction

Le but de cet exercice est de procéder à une mesure de performance d'une application parallèle.
La mesure de performance que nous allons utiliser est celle du speedup (accéleration).
Veuillez vous référer à votre cours pour avoir une définition du speedup.

Un autre but de cette série est de vous préparer aux projets.
En effet, nous allons vous demander de mesurer la performance de vos implémentations parallèles.
Avec cette série, vous réaliserez que procéder à des mesures de performance peut être fastidieux et/ou long: temps d'attente, mesure, collecte des résultats, etc.
Vous poserez donc les fondations de scripts qui serviront de base pour vos projets et faciliteront leur réalisation.

## Exercice 1: mesure de temps d'une exécution MPI

Nous vous donnons un code parallèle à compiler et exécuter sur Baobab.
Ce code, le fichier [`mmm.c`](src/mmm.c), se trouve dans le répertoire `src` de ce même dépôt.
Notez que le code parallèle fourni est trivial et n'a pas vraiment d'intéret.
Cette série s'articule autour de ce qu'il y aurait à réaliser en plus de la réalisation d'un code parallèle lors de vos projets.

Lisez ce code et modifiez-le, si nécessaire, de manière à pouvoir mesurer son temps d'exécution.
Pour cela, nous vous suggérons de jeter un oeil à la fonction [MPI_Wtime](https://www.open-mpi.org/doc/v3.0/man3/MPI_Wtime.3.php).

## Exercice 2: exécution de jobs en batch

Vous devez exécuter ce code avec un différent nombre de processeurs `p` de votre choix.
Par exemple les puissances de 2: 2, 4, 8, 16, 32, 64, ... en général on aime bien les puissances de deux en informatique, mais ce n'est de loin pas une règle absolue pour vos exécutions.

Lors de ces exécutions, vous devez mesurer le temps d'exécution parallèle pour vos différentes valeurs de `p`.
Vous devez aussi exécuter vos mesures plusieurs fois pour chaque `p`. En effet, comme pour toute mesure, vous devez calculer une moyenne et un écart type sur `k` mesures pour chaque `p` choisi.

Vous devez donc:
- Modifier le code pour que obtenir une mesure de temps d'exécution de votre application parallèle.
- Ecrire un script de soumission pour soumettre facilement vos job en série.
- N'oubliez pas que pour le calcul du speedup, `p = 1` doit aussi être mesuré.
- Faites également varier la taille du problème `m`, ce qui ajoute une dimension dans vos mesures.

Utilisez les valeurs suivantes:
- `p`: 1 (évidement), 2, 4 et 8 coeurs;
- `m`: 100 et 250;
- `k`: 4 mesures.

Ce qui équivaut à un total de 32 jobs à exécuter.

Une fois ceci fait, le résultat n'est pas forcément immédiat. 
Mais si votre programme à été correctement testé, n'êtes pas obligé de rester scotcher à votre machine en attendant que Slurm et Baobab terminent vos exécutions.

### Note sur l'exécution séquentielle (càd. `p = 1`) 

Normalement on utilise l'implémentation séquentielle la plus performante.
Cette implémentation peut algorithmiquement être assez différente de votre implémentation parallèle.
Dans ce cours, donc pour les projets suivants, on va simplement se limiter à l'utilisation de l'implémentation parallèle, mais avec un nombre de processus MPI égale à 1.
Ce n'est pas la manière la plus académique de faire, mais pour ce cours cela sera suffisant.

## Exercice 3: courbes de speedup

Vous avez donc à présent plusieurs mesures de temps à disposition qui dépendent de `p` et de `n`.
Vous avez également pour chaque couple `p` et `n`, `k` mesures de temps.

Vous devez:
- calculer la moyenne et l'écart-type pour chaque couple `p`, `n`;
- calculer le speedup moyen pour chaque `p`, `n`;
Vous êtes libre d'utiliser le logiciel que vous souhaitez pour produire vos graphiques.
L'important est que vous puissiez ensuite exporter vos figures et les insérer dans un rapport.

Tracer une coupe de speedup avec en abscisse les `p` et en ordonnée le speedup, faite ceci pour un `n` de votre choix.
Voici un exemple de plot à réaliser:

<img src="img/speedup_plot.png"  width="600">

Notez que les valeurs de `p` et `n`, du speedup sont fantaisistes et uniquement à but d'illustration.

## Exercice 4: graphique de speedup

Afin de visualiser la variation de speedup en fonction de la taille du problème `n`, tracer une image (graphique 2D, i.e. une heat map) de vos speedup moyen avec sur l'axe horizontal les `p` et sur l'axe verticale les `n`.
Faites en sorte que les petites valeurs de `p` du graphique se retrouve du coté gauche de l'image.
Voici un exemple d'un tel grpahique:

<img src="img/speed_up_matrix.png"  width="600">

## Exercice 5: tableau de mesures

Finalement il est à noter qu'on vous demandera probablement aussi un tableau résumant vos mesure. Comme par exemple ci-dessous:
| Nb. proc | Speedup moyen |Temps d'exec (moyenne) [s] | Temps d'exec (dev. std.) [s] | Nb. d'exec |
| ---      | ---      | ---      | ---      | ---      |
| 1   | 1 | 103.4   | 2.8   | 10   |
| 2   | 1.9 | 52.8   | 1.8   | 8   |
| 4   | 3.5 | 28.7   | 2.8   | 12   |
| ...   |...| ...   | ...   | ...   |
| 32   | 2.3 | 4.4   | 0.4   | 10   |

En général on essaye d'avoir le même nombre d'exécution par `p` pour un `n` donné.
Mais selon certaines exécutions peuvent échouer: seg fault absurde, walltime atteint, maintenance de Baobab, etc.
Dans ce cas, mettez le vrai nombre d'exécutions accomplies.
N'oubliez pas de spécifier les unités (ici en seconde, ou `[s]`).

Mais sa réalisation ne présente que peu d'intérêt dans cette série. 
Il n'est donc pas nécessaire que vous le réalisez. 
Il s'agit plus de vous rendre attentif à une bonne manière de présenter ses résultats.
