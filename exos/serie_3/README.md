#  Série 3: Les communications globales

[[_TOC_]]

Au cours de la série précédente, vous avez mis en oeuvre des communications entre processeurs à travers des appels à des fonctions dites de communication point-à-point. 
Ces fonctions mettent toujours en contact exactement deux processus: un pour l'envoi et un pour la réception. 
Les communications collectives, quant à elles, établissent un lien entre un groupe de processus. 
Ce groupe est spécifié en MPI par le communicateur. 
Vous avez déjà utilisé le communicateur par défaut, `MPI_COMM_WORLD`, qui englobe tous les processus. Dans cette série, nous étudierons les fonctions de **communication globale** et les **communicateurs**. 

Il existe plusieurs types de fonction de communication globale: les _one-to-all_, _all-to-one_ et _all-to-all_, ce qui respectivement signifie:
* un à tous: un processus envoie un message à tous les autres
* tous à un: tous les processus envoient un message à un unique processus
* tous à tous: tous les processus envoient un message à tous les autres processus

## Le broadcast

Le broadcast (la diffusion) est l'opération où un unique processus _root_ envoie le **même** message à tous les autres processus:
```c
MPI_Bcast(
    void* data,
    int count,
    MPI_Datatype datatype,
    int root,
    MPI_Comm communicator
)
```
Premièrement, il est important de noter que **tous les processus exécutent la fonction de communication globale**.
Les paramètres ont la même sémantique que les fonctions de communication point-à-point déjà étudiées.
La différence est qu'il n'y a pas de source et de destination, mais une racine (`root`).
La racine correspond au processus qui envoie le message et cette information est suffisante.
En effet, il n'est pas nécessaire de mettre de destinataires pour l'expéditeur étant donné que les destinataires sont connus: ce sont ceux du communicateur.
Quant aux destinataires, ils ont aussi toute l'information nécessaire étant donné qu'ils sont informés que l'expéditeur est la racine.
Dans le cas de la racine, `data` correspond au buffer d'envoi et pour les autres processus cela correspond au buffer d'envoi.

## Le scatter et gather

Le scatter (la dispersion) est similaire au broadcast à la différence que la racine n'envoie pas les mêmes données à tous les autres processus.
En effet, le scatter envoie des tranches d'un tableau (_slices_) aux destinataires.
Chaque destinataire reçoit une tranche différente du tableau.
L'ordre des tranches envoyées correspond au rang des processus.

```c
MPI_Scatter(
    void* send_data,
    int send_count,
    MPI_Datatype send_datatype,
    void* recv_data,
    int recv_count,
    MPI_Datatype recv_datatype,
    int root,
    MPI_Comm communicator
)
```

Les trois premiers paramètres sont similaires à ceux du broadcast. 
Les trois suivants sont similaires aux trois précédents, mais correspondent à la réception.
C'est une particularité du scatter par rapport au broadcast, on sépare les paramètres de l'envoi et de la réception.
Les deux derniers paramètres sont identiques 
au broadcast.

Normalement les paramètres `send_count`, `send_datatype`, `recv_count` et `recv_datatype` sont les même.
Néanmoins cela n'est pas indispensable, il suffit juste que les tailles des messages pour l'envoi et la réception doivent être égales.
Par exemple, si l'on est sur un sytème où les `double` font 8 bytes et les `integer` 4 bytes, la contrainte d'égalité de taille serait respecter avec les valeurs suivantes:
* `send_count`: 2, `send_datatype`: `MPI_DOUBLE`
* `recv_count`: 4, `recv_datatype`: `MPI_INT`

où les deux tailles correspondent à un message de 16 bytes.

Le gather (le rassemblement) est simplement l'opération inverse du scatter.
Tous les processus vont envoyer leur message à une racine qui assemblera les messages en un unique tableau.
L'ordre des tranches dans le tableau est également celui des rangs.

La signature de la fonction est claire si vous avez compris celle du scatter:

```c
MPI_Gather(
    void* send_data,
    int send_count,
    MPI_Datatype send_datatype,
    void* recv_data,
    int recv_count,
    MPI_Datatype recv_datatype,
    int root,
    MPI_Comm communicator
)
```

Néanmoins, il faut être attentif au fait que `recv_count` correspond à la taille de chaque message envoyé et non à la taille totale du tableau assemblé.

Les routines [MPI_Gatherv](https://www.open-mpi.org/doc/current/man3/MPI_Gatherv.3.php) et [MPI_Scatterv](https://www.open-mpi.org/doc/current/man3/MPI_Scatterv.3.php) étendent la fonctionnalité des routine [MPI_Gather](https://www.open-mpi.org/doc/current/man3/MPI_Gather.3.php) et [MPI_Scatter](https://www.open-mpi.org/doc/current/man3/MPI_Scatter.3.php) en permettant de faire varier le nombre de données reçue ou envoyer depuis chaque processus.
Jetez un oeil à leurs signatures et tentez de comprendre comment elles fonctionnent.

## Les communicateurs MPI

Un communicateur est une structure qui permet à MPI de dialoguer avec un ensemble de processus.
Le communicateur est donc utiliser pour sélectionner une paire ou l'ensemble de processus qu'il englobe.
Le communicateur par défaut est `MPI_COMM_WORLD` et ce dernier englobe tous les processus de l'application parallèle.

Il est possible de définir des communicateurs qui englobent uniquement un sous-ensemble des processus du `MPI_COMM_WORLD`.
Une approche possible est celle du _split_ ou division.
L'idée est que l'on va prendre un communicateur et séparer les processus en sous-ensembles disjoint.

La routine pour appliquer un split sur un communicateur `comm` afin de créer un nouveau communicateur `newComm` est:
```c
MPI_Comm_split(
	MPI_Comm comm,
	int color,
	int key,
	MPI_Comm* newComm
)
```
où le paramètre important est `color`.
Les processus appelant cette routine avec la même valeur de `color` feront partie du même communicateur.
La clé ordonne le rang du processus dans le nouveau communicateur.
S'il y a égalité des clés, le rang relatif utilisé est celui de l'ancien communicateur.
Ceci implique que si vous passez la même clé (`key`) à tous les processus, ils garderont le même ordre, mais avec des valeurs différentes dans le nouveau communicateur.

Par exemple, supposons que nous souhaitions que nous souhaitons créer le communicateur suivant.
Nous avons huit processus que nous souhaitons diviser en deux groupes: rouge et bleu.
Les quatre premiers processus doivent être dans le groupe rouge et les quatre suivant dans le groupe bleu:

<img src="fig/g1824.png" width="30%" height="30%">

On doit donc procéder à un split du communicateur `MPI_COMM_WORLD` avec deux couleurs:

```c
MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
MPI_Comm_size(MPI_COMM_WORLD, &nProc);

const int RED = 1;
const int BLUE = 2;

// groupe rouge ou groupe bleu ?
int color = BLUE;
if (myRank < 4) {
    color = RED;    
}

// on divise le communicateur "monde" et on garde le rang 
// relatif des processus
MPI_Comm myComm;
MPI_Comm_split(MPI_COMM_WORLD, color, nProc, &myComm);
```
et comme les autres ressources MPI, lorsqu'elles ne sont plus nécessaires, on les libère:
```c
MPI_Comm_free(&myComm);
```

Il est donc possible ensuite d'utiliser les fonctions de communication globales mais uniquement avec un sous ensemble de processus:

```c
if (myRank < 4) {
    MPI_Scatter(data, 10, MPI_DOUBLE, 10, buffer, MPI_DOUBLE, 0, myComm);
}
```
que l'on peut illustrer de la sorte:

![selected-scatter](fig/g30150.png)

où les données du scatter sont représentées par les carrés de couleurs.

Finalement, notez que le communicateur `MPI_COMM_WORLD` est **toujours disponible**.

# Exercice 1: le broadcast

Écrivez une fonction:
```c
HEPIA_Bcast(
    void* data,
    int count,
    MPI_Datatype datatype,
    int root,
    MPI_Comm communicator
)
```
qui procède à un broadcast à l'aide des fonctions `MPI_Send` et `MPI_Recv`.
Écrivez un programme simple dans lequel vous tester `HEPIA_Bcast` pour vous assurer qu'elle fonctionne.
Par exemple, vous pouvez écrire un programme où les processus affiche leur rang et la valeur reçue avant et après application du broadcast.
Tester votre envoi avec un scalaire et un tableau (vecteur, matrice, ou tenseur). 

Adapter votre programme pour qu'il utilise correctement la routine de MPI `MPI_Bcast` en place de votre fonction.

Ensuite faite un benchmark de votre programme: une fois avec `HEPIA_Bcast`, une autre avec `MPI_Bcast` en faisant varier le nombre de processus (`--ntask=1`, `--ntask=2`, `--ntask=4`, `--ntask=8`, ..., `--ntask=128`, ...).
Envoyez des messages suffisamment grand sur le réseau de manière à ce que les mesures soient significatives. 
Collecter les résultats et tracez les sous forme de graphique avec les nombres de processus en abscisse et le temps en ordonnée.
En fonction du nombre de processus p, quelle est la complexité de votre implémentation ?
Quelle est celle de MPI ?
Qu'en déduisez-vous ?

# Exercice 2: la moyenne des notes

Écrivez un programme qui calcule la moyenne des notes d'une classe.
On considère que le nombre d'élèves (càd de notes) `n` est connu ainsi que le nombre `p` de processeur.

Les étapes de l'algorithme sont les suivantes.
Pour plus de clarté nous supposons que `p <= n` et que `p` divise `n` dans les étapes suivantes:

1. un processus (disons `P0`) lit les notes,
2. le processus `P0` distribue équitablement le travail aux autres processus, càd que chaque processus `Pi` recevra `n/p` valeur à sommer localement,
3. tous les processus (y compris `P0` calculent leur somme locale),
4. les processus envoient les résultats de leur somme locale à `P0` qui calcule la somme globale,
5. le processus `P0` finalise le calcul de la moyenne en divisant la somme globale par `n`.

## Le cas simple: p divise n

Calculez la moyenne des notes d'une classe en supposant que `p` divise `n`.
Pour faire cet exercice vous ne devez **pas** utiliser les routines de communication point-à-point, mais uniquement les routines de communication globales.
Vous utiliserez donc les fonctions suivantes:
- [MPI_Scatter](https://www.open-mpi.org/doc/current/man3/MPI_Scatter.3.php)
- [MPI_Gather](https://www.open-mpi.org/doc/current/man3/MPI_Gather.3.php)
- [MPI_Reduce](https://www.open-mpi.org/doc/current/man3/MPI_Reduce.3.php)

et réalisez deux variantes de cet exercice: une en utilisant une opération de réduction et une autre sans utiliser d'opérations de réduction.

## Le cas général: p ne divise pas n

Calculez la moyenne des notes d'une classe mais vous ne pouvez plus supposer que `p` divise `n` (cas général).
N'utilisez pas non plus d'opérations de réduction.
Jetez un coup d'oeil aux fonctions suivantes pour inspirer votre solution:
- [MPI_Scatterv](https://www.open-mpi.org/doc/current/man3/MPI_Scatterv.3.php)
- [MPI_Gatherv](https://www.open-mpi.org/doc/current/man3/MPI_Gatherv.3.php)

## Génération des notes
Ne procédez pas à une lecture de fichier pour obtenir les notes, mais générer une liste d'entiers pour simuler la lecture sur un processus.
Vous pouvez générer les notes avec le code suivant:
```c
// pour les fonction srand et rand
#include <stdlib.h>
// pour la fonction time
#include <time.h>
// initialise la graine aléatoire
srand(time(NULL));
int* notes = malloc(m*sizeof(int));
for (int i = 0; i < m; ++i) {
    // génère un entier aléatoire entre 1 et 10
    notes[i] = rand() % 6 + 1;
}
```

# Exercice 3: les communicateurs

Créer un communicateur `evenOddComm` qui regroupe entre eux les processus de rangs pairs et ceux de rang impairs.
Procédez ensuite à un broadcast du processus de rang le plus petit de chaque communicateur et afficher les valeurs reçues pour vous assurer que votre application fonctionne. On peut illustrer un tel broadcast dans `MPI_COMM_WORLD` avec l'illustration suivante lorsque huit processus sont utilisés:

![selected-broadcast](fig/path1692.png)

