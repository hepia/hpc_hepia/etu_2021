# Corrections de la série 3

**Afin que la pédagogie fonctionne, nous vous conseillons de jeter un oeil à la correction uniquement si vous avez essayé d’accomplir la série par vous-même.**

## Exercice 1: le broadcast

```c
void HEPIA_Bcast(
  void* data,
  int count,
  MPI_Datatype datatype,
  int root,
  MPI_Comm communicator
) {
  if (myRank != root)
    MPI_Recv(data, datatype, count, root, STATUS, communicator);
  else
    for (int p = 0; p < nProc; p++) {
      if (myRank != p)
        MPI_Send(data, datatype, count, p, MPI_COMM_WORLD);
    }
}
```

## Exercice 2: la moyenne des notes

### Le cas simple: p divise n (avec reduction)

```c
MPI_INITIALIZE()

int* notes
if (myRank == 0)
   notes = generateNotes(nbNotes)

int nbNotes = n/nProc
int* myNotes = malloc(nbNotes*sizeof(int))

MPI_Scatter(notes, nbNotes, MPI_INT, myNotes, nbNotes, 
            MPI_INT, 0, MPI_COMM_WORLD)

int locSum = 0
for (int i = 0; i < nbNotes; i++)
  locSum += myNotes[i]

int totSum = 0
MPI_Reduce(&locSum, &totSum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD)

if (myRank == 0)
  int moyenne = totSum/n

MPI_FINALIZE()
```

### Le cas simple: p divise n (sans reduction)
```c
MPI_INITIALIZE()

double computeAverage(int* allSums, int n) {
  int totSum = 0
  for (int i = 0; i < allSums; i++) {
    totSum += allSums[i]
  }
  return (1.0*totSum)/n
}

int* notes
if (myRank == 0)
   notes = generateNotes(nbNotes)

int nbNotes = n/nProc
int* myNotes = malloc(nbNotes*sizeof(int))
MPI_Scatter(notes, nbNotes, MPI_INT, myNotes, nbNotes, 
            MPI_INT, 0, MPI_COMM_WORLD)

int locSum = 0
for (int i = 0; i < nbNotes; i++)
  locSum += myNotes[i]

int* allSums 

if (myRank == 0)
  allSums = malloc(nProc*sizeof(int))

MPI_Gather(&locSum, 1, MPI_INT, allSums, 1, MPI_INT, 0, MPI_COMM_WORLD)

if (myRank == 0)
  double moyenne = computeAverage(allSums, nProc)

MPI_FINALIZE()
```

## Le cas général: p ne divise pas n
```c
MPI_INITIALIZE()

int q = n/nProc
int r = n - nProc*q

for (int i = 0; i < nProc; i++) {
  if (r > 0) {
    counts[i] = q + 1
    r -= 1 
  } else {
    counts[i] = q
  }
  
  if (i == 0) {
    displ[i] = 0
  } else {
    displ[i] = displ[i-1] + counts[i-1]
  }
}

int nbNotes = counts[myRank]
int* myNotes = malloc(counts[myRank]*sizeof(int))

MPI_Scatterv(notes, counts, displ, MPI_INT, myNotes, nbNotes, 
             MPI_INT, 0, MPI_COMM_WORLD)

int locSum = 0
for (int i = 0; i < nbNotes; i++)
  locSum += myNotes[i]

MPI_Gather(&locSum, 1, MPI_INT, allSums, 1, MPI_INT, 0, MPI_COMM_WORLD)

if (myRank == 0)
  double moyenne = computeAverage(allSums, nProc)

MPI_FINALIZE()
```

# Exercice 3: les communicateurs

```c
const int ODD = 1
const int EVEN = 2

int color = ODD
if (myRank % 2 == 0)
  color = EVEN

MPI_Comm evenOddComm
MPI_Comm_split(MPI_COMM_WORLD, color, nProc, &evenOddComm)

MPI_Comm_rank(evenOddComm, &myOddEvenRank)

MPI_Bcast(&myRank, 1, MPI_INT, 0, evenOddComm)

MPI_Comm_free(&evenOddComm)
```
