**CORRECTION EN COURS D'EDITION**

# Correction de la série 6

Les trois exercice de cette série sont très similaires.
La principale difficulté consiste à faire une correspondance entre le domaine, la grille CUDA et les thread ids.

## Exercice 0: illustration 

Cet exercice n'était pas demandé.
Il s'agit d'une première illustration de l'utilisation des grilles et blocks 2D avec un mapping de thread id.
Ceci pour vous aider à comprendre l'indexation 2D d'une grille CUDA.

```c
#include <iostream>
#include <stdio.h>

// CUDA kernel illustrer les grilles 2D
__global__ void print_me(int N) {
  int ry = blockIdx.y*blockDim.y + threadIdx.y;
  int cx = blockIdx.x*blockDim.x + threadIdx.x;  
  // Thread ID globale
  int tid = ry*N + cx;
  printf(
    "Local id: (%d, %d), On block: (%d, %d), On row-col: (%d, %d), Global id: %d\n",
    threadIdx.y, threadIdx.x, 
    blockIdx.y, blockIdx.x, 
    ry, cx, tid
  );
}

int main() {
  const dim3 BLOCK_SIZE = dim3(1, 2);
  const dim3 GRID_SIZE = dim3(2, 3);

  std::cout << "BLOCK: " << BLOCK_SIZE.y << ", " << BLOCK_SIZE.x << std::endl;
  std::cout << "GRID: " << GRID_SIZE.y << ", " << GRID_SIZE.x << std::endl;
  // On lance le Kernel
  print_me<<<GRID_SIZE, BLOCK_SIZE>>>(GRID_SIZE.x*BLOCK_SIZE.x);
  // Et on attend
  cudaDeviceSynchronize();
}
```


## Exercice 1: addition de vecteurs

Dans cet exercice on fait correspondre les vecteurs 1D sur une grille CUDA en 2D.
Cela implique qu'il est nécessaire de calculer un thread id en 1D pour accéder aux valeurs des vecteurs et aussi de prendre en compte le cas ou un thread doivent calculer plus d'une seule valeur dans le kernel.

```c
#include <cassert>
#include<stdio.h>

// CUDA fct pour le thread id
__device__ int getGlobalIdx_2D_2D() {
  int blockId = blockIdx.x + blockIdx.y * gridDim.x;
  int threadId = blockId * (blockDim.x * blockDim.y) + (threadIdx.y * blockDim.x) + threadIdx.x;
  return threadId; 
}

// CUDA kernel pour l'addition de vecteurs
__global__ void vectorAdd(const int* a, const int* b, int* c, int totThreads, int N) {  
  // Thread ID globale
  int tid = getGlobalIdx_2D_2D();
  int nElems = ceil((1.0*N)/totThreads);
  for (int k = 0; k < nElems; k++) {    
    int i = tid*nElems + k;
    if (i < N) {
      c[i] = a[i] + b[i];
    }
  }
}

int main() {
  const int N = 1 << 20;
  // les vecteurs doivent tenir dans la mémoire du GPU
  const size_t n_bytes = sizeof(int)*N;  
  printf("Number of integers: %d\n", N);
  printf("Vector size: %f [GB]\n", (double) n_bytes/1000000000.0);

  int* h_a = (int*) malloc(n_bytes);
  int* h_b = (int*) malloc(n_bytes);
  int* h_c = (int*) malloc(n_bytes);

  // Du random: 0 à 99
  for (int i = 0; i < N; i++) {
    h_a[i] = rand() % 100;
    h_b[i] = rand() % 100;
  }

  // allocation sur le device
  int* d_a; 
  int* d_b;
  int* d_c;
  cudaMalloc(&d_a, n_bytes);
  cudaMalloc(&d_b, n_bytes);
  cudaMalloc(&d_c, n_bytes);

  // Host -> Device
  cudaMemcpy(d_a, h_a, n_bytes, cudaMemcpyHostToDevice);
  cudaMemcpy(d_b, h_b, n_bytes, cudaMemcpyHostToDevice);

  // Threads par block (on utilise le max: 1024)
  int num_threads = 1024;
  dim3 dimGrid (20, 20, 1);
  dim3 dimBlock (30, 30, 1);
  int totThreads = dimGrid.x*dimGrid.y*dimBlock.x*dimBlock.y;
  printf("Total thread available: %d. Total values to add: %d\n", totThreads, N);
  // On lance le Kernel: on note que c'est asynchrone
  vectorAdd<<<dimGrid, dimBlock>>>(d_a, d_b, d_c, totThreads, N);

  // Device -> Host: on rammène C et on note que cudaMemcpy est synchrone
  cudaMemcpy(h_c, d_c, n_bytes, cudaMemcpyDeviceToHost);

  // Tout est ok ?
  for (int i = 0; i < N; i++) {
    assert(h_c[i] == h_a[i] + h_b[i]);
  }

  // On libère la mémoire
  free(h_a);
  free(h_b);
  free(h_c);

  cudaFree(d_a);
  cudaFree(d_b);
  cudaFree(d_c);

  printf("Addition de vecteurs: OK\n");

  return 0;
}
```

## Exercice 2: correction de luminosité d'une image

Cet exercice est très similaire au précédent. 
Mais pour simplifier, on a supposé ici que l'image avait une taille où les pixels correspondent aux points de la grille CUDA (càd en terme de blocks et de grille).
C'est une correpondance simple 2D <-> 2D.

Pour un exemple de taille de grille qui ne correspond pas, c'est-à-dire qu'il y a soit trop de threads, soit pas assez, veuillez vous référer à l'exercice 2 où la gestion de tels cas est illustrée.

```c
#include<stdio.h>

__device__ unsigned int t_gamma(unsigned int pixel, double gamma) {
  double pixel_norm = ((double) pixel)/255.0;
  double new_pixel = 255.0*pow(pixel_norm, gamma);
  return rint(new_pixel);
}

__global__ void my_ker(unsigned int* img, int nRows, int nCols) {
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  int j = blockIdx.y * blockDim.y + threadIdx.y;
  if (i >= nRows || j >= nCols)
    return;
  // simple test: with gamme = 1, the image should stay the same
  img[i*nCols + j] = t_gamma(img[i*nCols + j], 1.0);
}

void print_img(unsigned int* img, int nRows, int nCols) {
  for (int i = 0; i < nRows; i++) {
    for (int j = 0; j < nCols; j++) {
      printf("%d, ", img[i*nCols + j]);
    }
    printf("\n");
  }
}

int main() {
  int NUM_ROWS = 20;
  int NUM_COLS = 20;
  int imgSize = NUM_ROWS*NUM_COLS*sizeof(unsigned int);

  unsigned int* h_img = (unsigned int*) malloc(imgSize);
  unsigned int* d_img;  
  // une image random en niveau de gris
  for (int i = 0; i < NUM_ROWS; i++) {
    for (int j = 0; j < NUM_COLS; j++) {
      h_img[i*NUM_COLS + j] = rand() % 255;
    }
  }

  // Image AVANT le réhaussement
  printf("BEFORE GAMMA CORR\n");
  print_img(h_img, NUM_ROWS, NUM_COLS);

  cudaMalloc(&d_img, imgSize);
  cudaMemcpy(d_img, h_img, imgSize, cudaMemcpyHostToDevice);

  dim3 dimGrid (2, 2, 1);
  dim3 dimBlock (10, 10, 1);
  my_ker<<<dimGrid, dimBlock>>>(d_img, NUM_ROWS, NUM_COLS);

  cudaMemcpy(h_img, d_img, imgSize, cudaMemcpyDeviceToHost);
  cudaFree(d_img) ;

  // Image APRES le réhaussement
  printf("AFTER GAMMA CORR\n");
  print_img(h_img, NUM_ROWS, NUM_COLS);

  return 0;
}
```
