# Corrections de la série

**Afin que la pédagogie fonctionne, nous vous conseillons de jeter un oeil à la correction uniquement si vous avez essayé d'accomplir la série par vous-même.**

Les corrections des exercices sont des pseudo-code.
Le but étant de laisser la possibilité à l'étudiant bloqué par certains exercices de pouvoir finaliser son implémentation.
Ceci afin de se familiariser avec l'environnement de développement MPI.

Les corrections sont donc un **pseudo code**, vous ne pouvez donc **pas les compiler**.
Notez aussi que les routines d'initialisation ainsi que les arguments non essentiels des routines MPI peuvent être soit absents, soit remplacé par des _placeholders_.
Par exemple:
```c
MPI_Init(&argc, &argv);
MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
MPI_Comm_size(MPI_COMM_WORLD, &nProc); 
MPI_Status status;
```
devient:
```
MPI_INITIALIZE()
```
ou peut simplement être omis.
On supposera que l'on aura accès à une variable indiquant le rang et une autre la taille du communicateur.

## Exercice 1: le ping-pong


```c
MPI_INITIALIZE()

bool ping = true
int other = 1
int nIter = 10
int BALL

if (myRank == 1) {
  ping  = false
  other = 0
}

while (0 < nIter) {
  if (ping) {
    MPI_Send(ball, MPI_INT, 1, other, MPI_COMM_WORLD)
  } 
  else {
    MPI_Recv(ball, MPI_INT, 1, other, STATUS, MPI_COMM_WORLD)
  }
  nIter -= 1
  ping = !ping
}

MPI_FINALIZE()
```

## Exercice 2: l'anneau

```c
MPI_INITIALIZE()

int token;

if (myRank == 0) {
  MPI_Send(token, MPI_INT, 1, 1, MPI_COMM_WORLD)
  MPI_Recv(token, MPI_INT, 1, nProc-1, STATUS, MPI_COMM_WORLD)
}
else {
  MPI_Recv(token, MPI_INT, 1, myRank-1, STATUS, MPI_COMM_WORLD)
  MPI_Send(token, MPI_INT, 1, (myRank+1)%nProc, MPI_COMM_WORLD)
}

MPI_FINALIZE()
```

## Exercice 3: la somme globale en parallèle

**ATTENTION: LE SCHÉMA DE L'ÉNNONCÉ PEUT PRÊTER À CONFUSION. NOUS AVONS PROPOSÉ DE CHANGER L'EXERCICE.**

Nous proposons d'appliquer une réduction telle que vue en cours.
Veuillez donc voir les slides du cours 2: gain et limitation de performace, cas où p divise n (et p < n).

On ne se préoccupe pas vraiment de la somme.
La réduction locale étant triviale, nous allons simplement sommer les rangs des process MPI en considérant que la somme locale a déjà eu lieu.

```c
MPI_INITIALIZE()

const int SENDR = 1;
const int RECVR = 2;

int getRole(int i) {
  int b &= 1U;
  if (b == 0) {
    return SENDR;
  } else {
    return RECVR;
  }
}

int flipBit(int i, int k) {
  i ^= 1UL << k;
  return i;
}
 
int nLevels = (int) log2(nProc);
int isPow2 = log2(nProc) - (int) log2(nProc) == 0;
int receiver;
int compute = 1;
int level = 0;

int degenerate_root = 0;
int degenerate_send = 0;

// calcul de somme locale trivial ...
int localSum = ...;
int buffer;

while (compute == 1) {  
  int myRole = getRole(myRank, gap);
  if (myRole == RECVR) {
    int src = flipBit(myRank, k);
    MPI_Recv(&buffer, 1, MPI_INT, src, 0, MPI_COMM_WORLD, &status);
    localSum += buffer;
  } else { // == SENDR, not really safe...
    int dest = flipBit(myRank, k);
    // it's maybe not a full binary tree
    if (dest > nProc - 1) {
      // I'm the largest but I send on the right...
      if (myRank == nProc - 1) {
        src = nProc - 2;
        MPI_Recv(&buffer, 1, MPI_INT, src, 0, MPI_COMM_WORLD, &status);
        localSum += buffer;
      } 
      // I send too far away...
      else {
        dest = nProc - 1;
        MPI_Send(&localSum, 1, MPI_INT, dest, 0, MPI_COMM_WORLD);
      }
    } 
    // normal send (reduce)
    else {
      MPI_Send(&localSum, 1, MPI_INT, dest, 0, MPI_COMM_WORLD);
      compute = 0;
    }
  }
  level = level + 1;
  if (level == nLevels) {    
    compute = 0;
  }
}
  
if (myRank == nProc - 1) {
 printf("Total sum is %d\n", localSum);
}
  
MPI_Finalize();
```

