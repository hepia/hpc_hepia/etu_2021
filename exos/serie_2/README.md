# Série 2: envoi et réception de messages

[[_TOC_]]

## Introduction

Comme son nom l'indique, MPI (`Message Passing Interface`) est une librairie faite pour échanger des messages entre processus.
La fonctionnalité de base avec MPI est donc de pouvoir envoyer et recevoir un message.
Ces échanges de messages permettent au processus de partager des données, des résultats, ou encore de se synchroniser. 
 
Les deux fonctions les plus élémentaires pour cela sont `MPI_Send(...)` et `MPI_Recv(...)` et servent respectivement à envoyer un message (le contenu d'un _buffer_) et à recevoir un message (qui sera écrit dans un _buffer_). 
Jetez un oeil à la [documentation de Open MPI](https://www.open-mpi.org/doc/current/) pour [MPI_Send](https://www.open-mpi.org/doc/current/man3/MPI_Send.3.php) et [MPI_Recv](https://www.open-mpi.org/doc/current/man3/MPI_Recv.3.php).
Ces deux fonctions font partie de la famille des fonctions de communication point-à-points.
C'est-à-dire qu'elles permette la communication uniquement entre deux processus.

Décortiquons un peu ces fonctions, en commençant par l'envoi de message:

```c++
MPI_Send(
    void* data,
    int count,
    MPI_Datatype datatype,
    int destination,
    int tag,
    MPI_Comm communicator
)
```
Pour envoyer un message, il est nécessaire de renseigner sur les données à envoyer (attention, c'est un pointeur `void`), le nombre d'éléments qui composent cette donnée, le type de la donnée (nécessaire pour calculer la taille du message), et le destinataire.
Le tag sera abordé dans une série ultérieure, et le communicateur doit toujours être passé pour que la fonction puisse trouver le destinataire.

L'envoi de message fonctionne d'une manière similaire:
```c++
MPI_Recv(
    void* data,
    int count,
    MPI_Datatype datatype,
    int source,
    int tag,
    MPI_Comm communicator,
    MPI_Status* status
)
```
où les trois premier paramètre sont identique à l'envoi.
En effet, MPI doit savoir où il doit stocker le contenu reçu et donc préparer assez de place pour le faire.
Dans le cas de MPI, on doit également savoir de qui on va recevoir un message.
Les messages reçus ne correspondant pas à l'émetteur attendu seront ignorés.
Le statut est une structure que nous allons pour le moment ignorer.

À chaque envoi (`MPI_Send`) doit correspondre à une réception (`MPI_Recv`).
Vous pouvez considérer que lorsqu'un processus envoi un message, ce dernier attendra que le message soit reçu par le destinataire avant de continuer sa propre exécution.
Vous pouvez également considérer qu'un processus exécutant une réception attendra que le message soit bien reçu avant de continuer son exécution.
Notez qu'il s'agit d'une simplification de la manière dont l'échange de message fonctionne avec MPI.
Nous verrons ceci plus en détail lorsque nous étudierons la sémantique des communications point-à-point.

On se rend également compte que MPI possède ses propres types de données (`MPI_Datatype`).
Ces derniers ressemble à ceux en C/C++ et son accessible via l'inclusion du header MPI (`mpi.h`).
Voici la liste des types MPI:

| MPI datatype | Équivalent C/C++ |
| --- | --- |
| MPI_SHORT | short int |
| MPI_INT | int |
| MPI_LONG | long int |
| MPI_LONG_LONG | long long int |
| MPI_UNSIGNED_CHAR | unsigned char |
| MPI_UNSIGNED_SHORT | unsigned short int |
| MPI_UNSIGNED | unsigned int |
| MPI_UNSIGNED_LONG | unsigned long int |
| MPI_UNSIGNED_LONG_LONG | unsigned long long int |
| MPI_FLOAT | float |
| MPI_DOUBLE | double |
| MPI_LONG_DOUBLE | long double |
| MPI_BYTE | char |

Avec ces informations, vous avez assez de connaissances pour comprendre et écrire votre premier programme dans lequel des processus peuvent communiquer.

## Exercice 1: le ping-pong

Écrivez un ping-pong entre deux processus.
Dans notre ping-pong les joueurs sont des processus, les raquettes `MPI_Send` et `MPI_Recv` et la balle un tableau d'entiers (arbitraires).
Le tableau doit contenenir 100'000 d'entiers. Testez également avec une balle composée de 1'000'000 d'entiers.

Un processus envoie donc un entier arbitraire à l'autre processus.
Une fois cet entier reçu, le processus retourne la valeur à l'envoyeur.
L'échange dure un maximum de `k` échanges et vous pouvez coder `k` en dur dans votre programme.
Aucun squelette de code n'est fourni, mais vous pouvez vous inspirer de ceux de la série précédente pour commencer à travailler.
Réfléchissez bien avant de commencer à écrire du code.
Les programmes MPI peuvent se révéler très difficiles à déboguer, un crayon et du papier peuvent être utiles avant d'utiliser son éditeur de texte favori.

Le programme doit fonctionner uniquement avec deux processus.
Si plus de deux processus existent, vous devez interrompre l'exécution de votre programme.
Nous vous suggérons d'utiliser la fonction [MPI_Abort(...)](https://www.open-mpi.org/doc/current/man3/MPI_Abort.3.php).

## Exercice 2: l'anneau

Écrivez un programme qui procède à un échange de message sur un anneau.
Dans le cas de cet exercice, nous nous référerons au message par token.
L'échange fonctionne de la manière suivante: le processus de rang `0` initialise le token et l'envoie au processus de rang `1`.
Le processus de rang `1` reçoit le message et l'envoie au processus de rang `2`.
Le processus de rang `3` reçoit le message et l'envoie au processus de rang `4`.
Et ainsi de suite jusqu'à ce que le processus de rang `N-1` reçoive le message que ce dernier renvoie au processus de rang `0` pour terminer la bouche (d'où l'anneau).

Voici une illustration du parcours du token (rond rouge) sur l'anneau en utilisant cinq processus. Les processus sont représentés par `P0`, `P1`, ..., `P4`:

![the-ring](fig/g1644.png)

### Aide pour réaliser l'exercice

Vous pouvez créer le token à passer de la manière suivante, ceci générera un entier aléatoire entre 1 et 10:
```c
// pour les fonction srand et rand
#include <stdlib.h>
// pour la fonction time
#include <time.h>
// initialise la graine aléatoire
srand(time(NULL));

// génère un entier aléatoire entre 1 et 10  
int i = rand() % 10 + 1;
```

Votre programme doit pouvoir fonctionner avec un nombre arbitraire de processus.

## Exercice 3: la somme globale en parallèle

Reprenez la série 1 et tenter d'implémenter la somme de vecteur en parallèle. 
Vous devez sommer `n` nombre en parallèle avec p unités de calcul.
L'algorithme suit le schéma suivant:

![parallel-sum](fig/g6056.png)

C'est à dire qu'on ne communique plus directement avec son "voisin" de gauche, mais uniquement avec les rang ayant la même parité.
Notez que ceci n'étant plus valable pour la dernière opération de réduction, où un process de rang impair va communiquer avec un process de rang pair.
Pour réaliser cet exercice, supposez que `p` est une puissance de deux.

### Aide pour réaliser l'exercice

Selon votre implantation, il est également possible que vous ayez besoin de synchroniser les processus. Pour ceci vous pouvez utiliser une barrière: [MPI_Barrier(...)](https://www.open-mpi.org/doc/v3.0/man3/MPI_Barrier.3.php), cette dernière a la même sémantique qu'une barrière dans un environnement multi-threads.

Vous pouvez supposer qu'au début du programme les `n/p` valeurs locales se trouve déjà sur l'unité de calcul.
Vous pouvez générer les données en insérant le _snippet_ suivant dans votre code:
```c
int m = n/p;
int* v = malloc(m*sizeof(int));
for (int i = 0; i < m; ++i) {
    v[i] = myRank;
}
```
et n'oubliez pas de libérer la mémoire lorsque vous n'avez plus besoin du vecteur:
```c
free(v);
```

