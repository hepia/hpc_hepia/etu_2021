# Série 1: premiers pas avec MPI

[[_TOC_]]

## Introduction

Le but de cette première série est de découvrir le principe de la programmation parallèle à l'aide d'une librairie [MPI](https://en.wikipedia.org/wiki/Message_Passing_Interface) (Message Passing Interface).
À l'issue de cette série, vous devriez avoir bien compris la notion de processus et rang avec MPI sur une architecture à mémoire distribuée.

Pour la réalisation de cette série d'exercices, vous utiliserez les postes de travail de l'HEPIA.
Les exemples et exercices seront écrits/réalisés en C.
La librairie Open MPI ainsi que les compilateurs dédiés sont installés sur ces machines.
Dès la prochaine série d'exercices, nous travaillerons sur la machine Baobab qui est le cluster de l'université de Genève.

## Exercice 0: Baobab ist kaput

Si les étudiants de l'HEPIA ne peuvent toujours pas accéder aux ressources de l'UniGE (càd le cluster baobab), vous pouvez quand même travailler avec Open MPI en l'installant sur votre machine.
En effet, vos machines mulit-cores sont aussi des machines parallèles.

Voici les étapes à suivre pour l'installer.
Ces étapes n'ont pas vocation de tutoriel absolu.
Vous pouvez utiliser l'approche la plus adaptée à votre distribution.
Sur Ubuntu, vous pouvez installer les packages:
```
mpi-default-bin
mpi-default-dev
```

En tant que futur informaticien, vous devez être conscient des commandes que vous éxécuter sur vos propre machines.

1. Télécharger Open MPI: [la v4.1 est disponible ici](https://www.open-mpi.org/software/ompi/v4.1/).
2. Extraire l'archive: tar -jxf openmpi-4.1.0.tar.bz2
3. Configurer, compiler et installer (le répertoire est donnée à titre d'exemple):
```
./configure --prefix=$HOME/lib/openmpi
make all
make install
```
4. Mettre à jour les paths (avec le répertoire donné en example):
```
PATH=\$PATH:\$HOME/lib/openmpi/bin"
LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:\$HOME/lib/openmpi/lib"
```

## Exercice 1: initialisation, taille et rang

Les concepts importants de cet exercice sont
- la compilation d'un code MPI sur Baobab (càd. avec les modules)
- l'exécution d'une code MPI sur Baobab (càd. avec SLURM)
- comprendre le concept de rang MPI.

Vous compilerez et exécuterez donc le code `hello.c` ci-dessous.
Lisez attentivement le code et les instructions qui suivent. 

```c
#include <mpi.h>
#include <stdio.h>

int main(int argc, char **argv) {
  int myRank;
  int nProc;
 
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  MPI_Comm_size(MPI_COMM_WORLD, &nProc); 
 
  printf("Hello, I'm %d out of %d\n", myRank, nProc);
 
  MPI_Finalize();
}
```

Nous vous suggérons de vous documenter sur les fonctions MPI utilisées dans ce code:

* [`MPI_Init`](https://www.open-mpi.org/doc/current/man3/MPI_Init.3.php)
* [`MPI_Comm_size`](https://www.open-mpi.org/doc/current/man3/MPI_Comm_size.3.php)
* [`MPI_Comm_rank`](https://www.open-mpi.org/doc/current/man3/MPI_Comm_rank.3.php)
* [`MPI_Finalize`](https://www.open-mpi.org/doc/current/man3/MPI_Finalize.3.php)

Jetez également un coup d'oeil à la [documentation Open MPI](https://www.open-mpi.org/doc/current/) et habituez-vous à l'utiliser, vous en aurez souvent besoin.

Essayez de prédire ce qui sera affiché.
Exéctuer ce code et faites varier le nombre de processus.

Est-ce que quelque chose vous surpend ? 
Que constatez-vous ? 
Répétez l'exécution plusieurs fois si nécessaire.

### Compiler son code MPI

La compilation se fait par le biais de la commande `mpicc`.
Cette dernière s'utilise de la même manière que le compilateur `GCC` (le compilateur MPI étant principalement un _wrapper_ de `GCC`):
```
mpicc hello.c -o hello
```
Ceci créera un exécutable nommé `hello`.
Notez que vous pouvez également utiliser les autres options disponibles avec `GCC`:
```
mpicc hello.c -o hello -std=c99 -Wall --pedantic
```

Il est à noter que **sur Baobab**, `mpicc` n'est pas disponible tant que le module `foss` n'est pas chargé:
```
module load foss
```

### Exécuter son programme MPI

L'exécution de votre application parallèle se fait de manière différente selon que vous utliserez SLURM (p. ex. sur Baobab), ou sur une quelconque machine (p. ex. votre laptop).

Sur **Baobab** (càd avec SLURM), l'exécution se fera **dans le script de soumission** avec la commande `srun`:
```
srun hello
```
Les paramètres correspondants aux ressources demandées sont automatiquement passé à `srun`.
Il n'est donc pas nécessaire de les repasser.

Un appel à `srun` dans votre script de soumission, comme décrit ci-dessus avec `hello`, correspond à une exécution de type **programme unique, donnée multiple** (SPMD - single program, multiple data).
Ceci démarre une application parallèle composée d'autant de processus que requis par votre script de soumission (p. ex. `ntasks=8`, voir le script de soumission ci-dessous).
Le programme `srun` exécute autant de copies que nécessaire de la commande passée en paramètre, dans notre cas il s'agit de huit copies de l'exécutable `hello`.
Ces exécutables feront partie du même "environnement parallèle".
L'exécution SPMD est celle que nous utiliserons le plus souvent (si ce n'est tout le temps) au cours de ce semestre.

Ensuite lisez ce script de soumission:
```bash
#!/bin/sh
#SBATCH --job-name=hello
#SBATCH --output=hello.o%j
#SBATCH --ntasks=8
#SBATCH --partition=debug-cpu
#SBATCH --time=00:05:00

echo $SLURM_NODELIST

srun ./hello
```

Sur une **machine quelconque**, vous ferez appelle à la commande `mpirun` suivit du nombre de processus requis pour votre application prallèle.
En effet, sans script de soumission il faut spécifier l'équivalent de `ntasks=N`.
Ceci se fait par le biais de l'option `-n` suivit du nombre de processus à exécuter:
```
mpirun -n 8 hello
```
Notez que nous voulons que vous apprenez à utiliser un cluster. 
Vous devez donc privilégier l'utilsation de Baobab et SLURM.

## Exercice 2: la somme locale de vecteurs

À présent que l'exécution d'un code MPI n'a plus de secrets pour vous, effectuez une opération en parallèle en copiant le programme suivant.
Mais avant tout, lisez-le et tentez de prédire ce qui va se passer. 

Ensuite, exécutez ce programme et assurez-vous de comprendre le résultat de l'exécution.
Faites varier le nombre de processus créés.

```c
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  int myRank;
  int nProc;
 
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  MPI_Comm_size(MPI_COMM_WORLD, &nProc);
 
  // initialise un vecteur de taille 10 dont chaque
  // composante vaut myRank
  const int n = 10;
  int* v = malloc(n*sizeof(int));
  for (int i = 0; i < n; ++i) {
      v[i] = myRank;
  }  

  int sum = 0;
  for (int i = 0; i < n; ++i) {
      sum += v[i];
  }

  if (myRank % 2 == 0) {
    printf("I'm process %d", myRank);
    printf(", and the local sum is %d\n", sum);
  }

  free(v);
  MPI_Finalize();
}
```

