# Explications de l'exercice 1

**Pour que la pédagogie fonctionne, ne lisez cette partie uniquement si vous avez essayé de faire l'exercice !**

Les exemples ci-dessous sont illustrés avec `mpirun`:
```
mpirun -np N ./my_exec 
```
où `N` correspond au nombre de tâches à éxécuter.
Cela serait le cas sur une machine quelconque (p.ex. la votre).

L'équivalent `sbatch` pour cette commande serait de faire un submit script:
```
#SBATCH --ntasks=N
#SBATCH --partition=debug
#SBATCH --time=00:05:00

srun ./my_exec
```

On peut aussi utilser directement la commande `sbatch` avec les paramètres de type `#SBATCH --flag=x` directement passés à la ligne de commande.

Mais l'écrire pour chacune des exécutions suivantes serait verbeux et peu lisible.
Nous allons donc "abstraire" la commande `sbatch` et son script par `mpirun`.

## Exécution
Il est possible de n'exécuter qu'un seul processus (quasiment similaire à une exécution normale):
```
$ mpirun -np 1 ./hello 
Hello, I'm 0
```
où l'on voit effectivement que le programme affiche son propre rang.
Mais cette exécution n'est pas très intéressante pour faire du parallélisme.
On peut donc tenter d'exécuter le programme avec deux processus:
```
$ mpirun -np 2 ./hello
Hello, I'm 0
Hello, I'm 1
```
ou bien sept:
```
$ mpirun -np 7 ./hello
Hello, I'm 6
Hello, I'm 0
Hello, I'm 1
Hello, I'm 2
Hello, I'm 3
Hello, I'm 4
Hello, I'm 5
```
Et on se rend compte que tous les processus ont affiché leur propre rang sur la sortie standard.
Vous noterez que vos exécutions n'affichent pas le même ordre.
Ceci est parfaitement normal.
En effet, vous ne pouvez pas prévoir dans quel ordre les processus vont pouvoir écrire sur la sortie standard, étant donné que vous ne savez pas exactement quand ces derniers exécuteront leurs instructions.
La seule chose dont vous êtes sûr c'est que les instructions seront exécutées dans l'ordre (comme n'importe quel programme séquentiel), mais vous ne savez pas quand elles s'exécuteront.
**Ne supposez donc jamais que vos processus s'exécutent exactement en même temps !**

## Le programme

Lors d'un appel à `MPI_Init` toutes les variables et structures nécessaires à MPI sont initialisées. 
On peut, par exemple, citer le communicateur MPI (`MPI_COMM_WORLD`) ainsi que l'assignation des rangs (uniques) aux différents processus. 

Les deux fonctions suivantes permettent de renseigner sur le nombre de processus ainsi que le rang d'un processus.
`MPI_Comm_size` assigne dans une variable la taille du communicateur.
La taille du communicateur, lorsque `MPI_COMM_WORLD` est utilisé, correspond au nombre de processus crées.
la fonction `MPI_Comm_rank` assigne le rang d'un processus appartenant au communicateur utilisé.
Les rangs sont assigné de `0` à `N-1`, `N` étant la taille du communicateur utilisé.
Le but du rang étant de pouvoir identifier les processus lors des étapes de communication entre les différents processus de l'application parallèle.

Finalement, à la fin de chaque programme, il est *indispensable* de d'appeler la fonction `MPI_Finalize`.
Cette fonction nettoie l’environnement MPI (un peu comme à la manière d'un `free`).
Une fois cette fonction appelée, il est impossible d'appeler d'autres fonctions de la librairie MPI.
N'oubliez jamais d'appeler cette fonction, sans quoi, votre programme peut avoir un comportement erratique.

# Explications de l'exercice 2

**Pour que la pédagogie fonctionne, ne lisez cette partie uniquement si vous avez essayé de faire l'exercice !**

Nous n'utilisons aucun nouvelle fonctionalité de MPI dans cet exemple.
Mais cette fois nous faisons usage des variables initialisées par MPI pour du calcul et de la logique.

# Exécution
Commençons par exécuter le code:
```
$ mpirun -np 7 ./sump
I'm process 0, and the local sum is 0
I'm process 2, and the local sum is 20
I'm process 4, and the local sum is 40
I'm process 6, and the local sum is 60
```
où l'on constate que nous avons sept processus mais seulement quatre affichent leur résultat sur la sortie standard.
De plus, chacun des processus affiche une somme différente.

Premièrement, chaque processus initialise un vecteur de taille 10 dont chacune des composante vaut son rang:
```c
const int n = 10;
int* v = malloc(n*sizeof(int));
for (int i = 0; i < n; ++i) {
    v[i] = myRank;
}
```
Donc dans cet exemple, même si chaque processus execute le même code, les données générées sont différentes:

* processus de rang 0: `[0, 0, ..., 0]`
* processus de rang 1: `[1, 1, ..., 1]`
* ... 
* processus de rang N-1: `[N-1, N-1, ..., N-1]`

En conséquence, la somme pour chaque processus sera `10*myRank` et ceci explique les différence dans les valeurs affichées.

Ensuite, on constate que seul un sous-ensemble des processus affichent leur somme.
Ceci est contrôlé par le test appliqué au branchement:
```c
if (myRank % 2 == 0)
```
Ce test vérifie si le rang du processus est pair, et s'il est pair, la somme est affichée.
Comprenez bien que chaque processus exécute le même code mais avec des valeurs potentiellement différentes. 
Donc chaque processus aura un comportement potentiellement différent des autres.
