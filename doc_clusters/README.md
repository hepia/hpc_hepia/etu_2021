[Documentation hpc unige](https://doc.eresearch.unige.ch/hpc/start)

[Documentation de slurm](https://slurm.schedmd.com/documentation.html)

Contact équipe HPC unige : hpc@unige.ch

## Login nodes

login2.baobab.hpc.unige.ch

login1.yggdrasil.hpc.unige.ch

## Script sbatch d'exemple

```
#!/bin/sh
#SBATCH --job-name=test
#SBATCH -n 10
#SBATCH --mem=0
#SBATCH --time=0-00:15:00
#SBATCH --partition=debug-cpu
#SBATCH --output=slurm-%J.out

echo $SLURM_NODELIST

srun hello
```

## Gestion des noeuds utilisés

Pour afficher les noeuds utilisés par un job à chaque exécution d'un job :

```
echo $SLURM_NODELIST
```

Pour exclure certains noeuds de l'exécution : 

```
#SBATCH --exclude=cpu[003,005,050-070],gpu[001,002]
```


## Mesure de performances

Pour mesurer les performances parallèles d'un code, il est important de s'assurer que toutes les exécutions se font sur des processeurs de même type afin d'obtenir des résultats cohérents.

Cette partie de la documentation hpc unige indique la configuration des noeuds des clusters : https://doc.eresearch.unige.ch/hpc/hpc_clusters#compute_nodes .

Par exemple, cette configuration permet de n'utiliser que des noeuds equipés de processeurs Xeon Gold 6240 @ 2.6GHz sur Yggdrasil :

```
#SBATCH --constraint=GOLD-6240
```

Cela peut néanmoins ralonger le temps d'attente avant d'obtenir des ressources pour exécuter le job vu que le scheduler a moins de liberté pour affecter le job. Pensez-y lorsque vous avez des deadlines !
