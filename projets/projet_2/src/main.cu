#include <cstdio>
#include "laplace_solver.cuh"

int main()
{
  // vous pouvez remplacer l'instruction suivante, elle est uniquement
  // présente pour que la démo/squelette compile/fonctionne
  const size_t N = 20, M = 10;
  float *north = (float *)malloc(sizeof(float) * M);
  float *south = (float *)malloc(sizeof(float) * M);
  float *east = (float *)malloc(sizeof(float) * (N - 2));
  float *west = (float *)malloc(sizeof(float) * (N - 2));
  float *heated = heat_solver(
      N, M, 100,
      north, south, east, west,
      dim3(1, 2, 1), dim3(10, 10, 1));
  // Test bidon pour être sûr que la fonction heat_solver
  // retourne bien un pointeur host.
  heated[0] -= 1.0e-120;
  if (heated[0] == north[0])
  {
    printf("Coin nord-est bon\n");
  }
  return 0;
}
