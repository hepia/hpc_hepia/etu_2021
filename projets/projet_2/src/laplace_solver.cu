#include <cstdio>

__global__ void dummy_kernel(float *domain)
{
  int tid = (blockIdx.x * blockDim.x) + threadIdx.x;
  printf("Cuda thread id %d is doing stuff on domain\n", tid);
}

// NE CHANGEZ PAS CETTE SIGNATURE ET NE DEPLACEZ PAS CETTE FONCTION !!!
float *heat_solver(
    int N, int M, int T,
    float *north, float *south, 
    float *east, float *west,
    dim3 grid_dim, dim3 block_dim
)
{
  // vous devez remplacer les instructions suivantes, elles sont uniquement
  // présentes spour que la démo/squelette fonctionne
  const size_t n_bytes = sizeof(float) * (N * M);
  float *d_domain;
  cudaMalloc(&d_domain, n_bytes);
  dummy_kernel<<<grid_dim, block_dim>>>(d_domain);
  cudaDeviceSynchronize();
  return (float *)malloc(n_bytes);
}
