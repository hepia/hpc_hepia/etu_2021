[[_TOC_]]

# Projet 2: l'équation de la chaleur sur accélerateur

Le but de ce projet **noté** est de procéder à une implémentation simple d'un code stencil qui résout l'équation de la chaleur sur un domaine carré en deux dimensions. 
Il s'agit du même problème que le projet précédent, sauf que l'exécution se fera sur [GPU](https://en.wikipedia.org/wiki/Graphics_processing_unit).

Le but de cette répétition est de:
- vous permettre de bien comparer deux modèles de programmation (c'est un bon exercice),
- de ne pas perdre de temps à comprendre un nouveau sujet et de vous concentrer sur la partie implémentation.


On ne revient pas sur l'algorithme, il a été introduit lors du [projet 1](https://gitlab.unige.ch/hepia/hpc_hepia/etu_2021/-/tree/main/projets/projet_1).

## Synchronisation des threads CUDA

On procède à un bref rappel sur la synchronisation des kernels et threads avec CUDA.

Premièrement, le lancement d'un kernel sur le device vis-à-vis de l'hôte est une opération asynchrone.
Il faut donc procéder à une opération de synchronisation pour que l'hôte attende sur le device.
L'utilisation de [cudaDeviceSynchronize](https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__DEVICE.html#group__CUDART__DEVICE_1g10e20b05a95f638a4071a655503df25d) utilisé sur le host pour qu'il attendre la completion des kernels sur le device. 
On rappelle que certaines opérations snychronisent implicitement le device et le host, comme [cudaMemcpy](https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__MEMORY.html#group__CUDART__MEMORY_1gc263dbe6574220cc776b45438fc351e8).


Ensuite, les threads ne sont synchrones que par warp, mais il est possible de facilement les synchroniser sur un même block avec la fonction  [__syncthreads](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#synchronization-functions).

On rappelle également que pour un stream donné, l'ordre des kernels soumis est préservé à l'exécution.
Mais l'exécution entre les différents streams est asynchrone. 

Finalement, les threads entre les blocks d'une grille sont **asynchrones**, mais il est possible de les synchroniser en utilisant les groupes coopératifs.
Toutefois pour réaliser ce travail, il n'est pas nécessaire de synchroniser la grille.
Mais les étudiants curieux ou les CUDA enthusiasts peuvent lire l'annexe optionelle à la fin de cet énoncé.

## Travail à réaliser

Votre travail consiste à accélérer ce code stencil en utilisant l'architecture du GPU et le modèle de programmation [CUDA](https://en.wikipedia.org/wiki/CUDA).
Vous devrez également procéder à des mesures de performances, mais celles-ci seront beaucoup plus simples que celles faites avec C et OpenMPI.

### Partie implémentation

On rappelle le pseudo-code séquentiel de la boucle temporelle/spatiale.
Le pseudo-code suivant illustre l'algorithme approximant la solution de l'équation de Laplace pour un nombre d'itérations fixe `T`, pour un domaine discret composé de `X` $`\times`$ `Y` points:

```c
U = init() // U a squared domain
for (int t = 0; t < T; t++) {
  for (int i = 0; i < X; i++) {
    for (int j = 0; j < Y; j++) {
      U_temp[i][j] = 0.25 * (
        U[i-1][j] + U[i+1][j] + 
        U[i][j-1] + U[i][j+1]
      )
    }
  } 
  U = U_temp
}
```

Votre travail sera réalisé sous la forme d'une "librairie".
Votre librairie sera incluse dans un autre code pour y être utilisée.
Vous n'avez donc pas besoin de rendre un point d'entrée pour votre projet.

Vous devez simplement utiliser et ajouter votre code dans le "kernel launcher" fourni, dont voici le prototype: 
```c
float* heat_solver(
  int N, int M, int T, 
  float* north, float* south, float* east, float* west,
  dim3 grid_dim, dim3 block_dim
);
```
qui se trouve dans fichier `laplace_solver.cu` dans le répertoire `src` à la racine de ce dépôt.
Où les arguments sont:

- `int N`: le nombre de lignes du domaine discret,
- `int M`: le nombre de colonnes du domaine discret,
- `int T`: le nombre d'itération à appliquer,
- `float* north` et `float* south`: les bords haut et bas du domaine, de taille M,
- `float* east` et `float* west`: les bords doit et gauche du domaine, mais attention ils seront de taille N-2,
- `dim3 grid_dim` et `dim3 block_dim`: les dimensions de la grille et des blocks. Notez qu'il n'est pas indispensable que tous les threads d'un block fassent du calcul. C'est-à-dire qu'il n'est pas nécessaire de trouver un mapping parfait de votre domaine en blocks.

**Ne changez pas la signature de cette fonction !
Ne renommez pas cette fonction !**

Dans ce même `laplace_solver.cu`, vous voyez également:
```c
__global__ void dummy_kernel(float *domain) { ... }
```
qui est un exemple de kernel **bidon**. 
Ceci pour vous donner une idée d'une structure possible.
Un header est également distribué pour l'utilisation de votre librairie.

**Lisez attentivement le contenu de ces fichiers. 
Le tout devrait être assez explicite.**
Posez des questions aux cadres du cours si nécessaire.

### Partie compilation

Nous vous donnons un squelette d'application avec un exemple de point d'entrée, `main.cu`, qui illustre comment votre code doit pouvoir être appelé par le correcteur.
Le point d'entrée fourni vous permet de tester que les consignes sont bien respectées.
Mais comme vous écrivez une "librairie", cela n'implique pas que c'est ce point d'entrée qui sera utilisé avec votre code.

Nous vous donnons également un `Makefile` qui sera utilisé pour la compilation et l'édition lien.
Vous devez notez les points suivants:

- il est est très important que votre librairie soit compilée comme `hepia_gpu_lib.o`. Ne modifiez pas ce nom, c'est avec cette librairie que l'édition de liens sera faite pour corriger votre travail.
- Dans le `Makefile` fourni cela correspond à la cible `lib`, **ne modifiez pas cette cible**, c'est cette dernière qui sera utilisée pour compiler votre librairie lors de la correction.
- Notez que le header `laplace_solver.cuh` sera également inclus par défaut, **ne modifiez donc pas le prototype** du "kernal launcher" `heat_solver(...)` dans ce fichier.

Dans votre rendu:
- il y aura un répertoire `src` à la racine du dépôt,
- tout votre code se trouvera dans le répertoire `src`, sans aucun sous-répertoire.

Lors de l'évaluation de votre travail, les modules chargés seront ceux du module CUDA:
```
$ module load CUDA
```
Et le compilateur à utiliser est évidemment [nvcc](https://en.wikipedia.org/wiki/Nvidia_CUDA_Compiler).
Une nouveauté par rapport au projet précédent, votre code doit compiler et fonctionner sur Baobab et Yggdrasil.

Pour résumer:
- tant que la cible `lib` produit un binaire  `hepia_gpu_lib.o` et qu'il contient la fonction décrite par le prototype du header `laplace_solver.cuh`, votre code pourra être testé lors de la correction.
- Vous avez également un "test 0" fournit dans le fichier `test_0.cu` dans `src` qui est un exemple de test qui pourrait être exécuté avec votre librairie. Il s'agit du résultat après zéro itération. Ce test devrait fonctionner si votre librairie est correctement implmentée. Attention, si ce test passe, cela **n'implique pas que votre code fonctionne** pour toute taille de domaine ou nombre d'itération. Lisez le contenu de ce fichier pour bien comprendre ce qui est testé. Ce test est compilé via la cible `test` du `Makefile` fournit.

### Partie mesures

Pour les mesures vous devrez simplement faire varier la taille du problème et mesurer son temps d'exécution.
Le travail étant délégué à un accélérateur, vous pouvez faire peu de mesures par run:
- limtez vous à cinq mesures par run,
- Pour la taille, limitez vous à trois catégories: petit, moyen et grande. 

Pour choisir ces tailles choissisez un modèle de GPU et fixez les tailles en fonction de la mémoire disponible sur GPU, par exemple:
- petit: 25-50% de la mémoire, 
- moyen: 50-75% de la mémoire,
- grande: 75-100% de la mémoire.

Fixez le nombre d'itérations et **n'utilisez pas** de critère de convergence pour mettre fin à votre exécution.

Pour mesurer votre temps d'exécution, faites simple: mesurez le temps d'appel à votre librairie.
C'est pour cette raison que vous devez faire assez d'itérations.

Si vous pensier parser le résultat du profiler, c'est possible. Mais notez que `nvprof` ajoute un petit overhead de performance, comme indiquer dans sa [documentation](https://docs.nvidia.com/cuda/profiler-users-guide/index.html).

**Pensez également à réserver correctement la mémoire sur l'hôte pour éviter que votre job ne soit tué par SLURM.**

### Partie rapport

Votre rapport sera au format MD déposé sur votre dépôt (voir ci-dessous, "Déposer votre travail") en tant `readme.MD` du projet. 
Ce fichier Markdown se trouvera donc à la racine du dépôt.
Le rapport devra contenir :
- une brève description de la manière dont vous déléguer le travail sur l'accélerateur. P.ex. quel est le découpage sur la grille ? Utilisez vous un ou plusieurs kernels ? Avec un ou plusieurs streams ? Etc.
- Une brève description de la manière dont vous avez fait vos mesures. Une ou deux phrases suffisent (p.ex. j'ai parsé le profile, j'ai fait un `time`, j'ai utilisé `clock_t`, etc.).
- Vos mesures de performance faites sur GPU. Avec le faible nombre de mesure à faire, un tableau suffit. Il n'est pas nécessaire de faire une courbe de speedup, ni de le calculer.

Aussi, il n'est **pas** nécessaire de :
- de redécrire l'équation de la chaleur elle-même dans votre rapport. Nous savons quel problème vous devez résoudre. Pas besoin d'ajouter des schéma du domaine, etc.
- De blablater à quel que ce projet est intéressant si vous ne le pensez pas. Ceci n'impacte pas votre note.
- De faire des mesures de performances sur les deux machines Baobab ou Yggdraqsil. **Choisissez en une seule et un seul modèle de GPU.** Vous trouverez le matériel disponible dans la [documentation des machines](https://doc.eresearch.unige.ch/hpc/hpc_clusters#for_advanced_users).

Soyez bref et simple, de manière à ce que cette partie ne soit pas trop chornophage pour vous.

Discutez également cette question dans votre rapport: 

**Comment comparer les performances entre un code parallèle sur CPU (que ce soit MPI, thread posix, OpenMP, etc.) et un code accéléré sur GPU ?**

C'est une question semi-ouverte, il n'y a pas de réponse absolue. On veut simplement lire une réponse d'ingénieur.

### Déposer votre travail

Votre travail est à rendre sur le dépôt Git déjà existant à votre nom dans le groupe suivant:
https://githepia.hesge.ch/hpc_2122_projet_2

C'est-à-dire si votre mail est `berk.prout@etu.hesge.ch`, votre dépôt assigné sera `berk.prout`.

Si vous ne voyez pas votre dépôt, ou que vous n'avez pas les droits d'écritures, signalez-le au plus vite.
N'attendez pas la dernière minute pour tester vos accès, faites ceci dès que vous aurez terminé la lecture de cet énoncé.

Vérifiez également que les personnes ayant accès à votre dépôt en plus de vous même sont :

- Christophe Charpilloz (christophe.charpilloz@hesge.ch)
- Pierre Künzli (pierre.kunzli@hesge.ch)
- Michaël El Kharroubi (michael.el-kharroubi@hesge.ch)

Vous pouvez vérifier cela via `project information -> members` dans le menu de gauche de l'interface GitLab.

### Délai

Le délai est fixé pour le **24 janvier 2022**.
Pour vous assurez que les consignes sont bien respectées, n'hésitez pas à interpeller un des cadres du cours pendant les séances d'exercices.
L'assistant a "autorité" concernant les consignes d'exécution, c'est ce dernier qui évalue et valide l'exécution de vos codes.

# Annexe: synchronisation de la grille (lecture optionelle)

La compréhension de cette section est **optionelle**, que ce soit pour le cours ou pour ce projet.
Elle est néanmoins fournie afin de compléter ce qui pourrait être une zone d'ombre sur les différents mécanisme de synchronisation avec CUDA.
Notez que les fonctions d'intérêt sont liées à la documentation CUDA dans ce document.

Les threads entre les blocks d'une grille sont **asynchrones**, mais il est possible de les synchroniser à l'aide de groupes coopératifs.
Ceci n'a pas été étudié en cours ou en exercices et est brièvement introduit dans cette section.
N'hésitez pas à vous référer à la [documentation CUDA](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html) pour compléter cette section si le sujet vous intéresse.

L'utilsation des groups coopératifs n'est pas compliquée en soit, mais elle nécessite de faire (très légèrement) du C++.
Or ce langage n'est pas un prérequis pour ce cours.
Elle nécessite également l'utilsation de [cudaLaunchCooperativeKernel](https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__EXECUTION.html#group__CUDART__EXECUTION_1g504b94170f83285c71031be6d5d15f73) pour éxécuter les kernels.

On crée un groupe avec `cooperative_groups::this_grid()`, de type `cooperative_groups::grid_group`.
Une fois se groupe crée, on le synchronise avec la méthode `sync()` du groupe `cooperative_groups::grid_group`.

Voici comment créer et utiliser un groupe sur une grille:
```c
#include <cuda.h> 
#include <cuda_runtime_api.h> 
#include <cooperative_groups.h>

__device__ void step_one(...) { ... }

__device__ void step_two(...) { ... }

__global__ void collab_kernel(...) {
  // création d'un group coopératif sur une grille
  cooperative_groups::grid_group g = cooperative_groups::this_grid();
  // premier step, asynchrone
  step_one(...);
  // synchronisation de la grille
  g.sync();
  // deuxième step, asynchrone
  step_two(...);
  // synchronisation de la grille
  g.sync();
}
```

Le kernel s'exécute pas directement, mais via un launcher coopératif:
```c
kernel_args = ...
dim3 dimBlock = dim3(1024, 1, 1);
dim3 dimGrid = dim3(32, 1, 1);
// le launcher prend en paramètre le kernel, les dimension de la grille 
// et les arguments du kernel
cudaLaunchCooperativeKernel((void*) collab_kernel, dimGrid, dimBlock, kernel_args);
```

La compilation doit également être adaptée:
```
nvcc -arch=sm_60 -rdc=true my_app.cu
```
où on précise l'architecture `sm_60` qui correspond à une architecture Pascal (càd CUDA 8 et plus).
En dessous de cette génération, la synchronisation des threads sur une grille n'est pas possible.

Un exemple complet vous est fournit dans ce snippet:
```c
#include <cuda_runtime_api.h> 
#include <cuda.h> 
#include <cooperative_groups.h>
#include <stdio.h>

struct KerParams { 
  int* ary; 
  int N;
  int T;
};


__device__ void step_one(int tid, int t) {
  printf("Thr id: %d, t: %d\n", tid, t);
}

__device__ void step_two(int* ary, int N, int tid) {
  for (int i = 0; i < N; i++) {
    printf("[%d] %d; ", tid, ary[i]);
  }
  printf("\n");
}

__global__ void collaborate(KerParams ker_args) {
  int tid = (blockIdx.x*blockDim.x) + threadIdx.x;
  cooperative_groups::grid_group g = cooperative_groups::this_grid();
  for (int t = 0; t < ker_args.T; t++) {
    step_one(tid, t);
    g.sync();
    step_two(ker_args.ary, ker_args.N, tid);
    g.sync();
  }
}

int main() {  
  int N = 10;
  int T = 2;

  size_t n_bytes = sizeof(int)*N;
  int* h_ary = (int*) malloc(n_bytes);
  for (int i = 0; i < N; i++) {
    h_ary[i] = i + 1;    
  }

  int* d_ary; 
  cudaMalloc(&d_ary, n_bytes);
  cudaMemcpy(d_ary, h_ary, n_bytes, cudaMemcpyHostToDevice);

  const int NUM_THREADS = 5;
  dim3 dimBlock = dim3(NUM_THREADS, 1, 1);
  dim3 dimGrid = dim3(N/NUM_THREADS, 1, 1);
  
  KerParams param { d_ary, N, T };
  void **kernel_args = (void**) malloc(sizeof(void*));
  kernel_args[0] = &param;
  cudaLaunchCooperativeKernel((void*) collaborate, dimGrid, dimBlock, kernel_args);
  
  cudaDeviceSynchronize();  
  cudaFree(d_ary);
  free(h_ary);
}
```
