[[_TOC_]]

# Question fréquentes des étudiants

Nous allons ajouter ici au fur et à mesures les questions fréquentes des étuidants.
Si vous avez un doute, jetez un oeil à cette FAQ avant de contacter les cadres du cours.
Peut-être que votre question a une réponse ci-dessous.

## Compatibilité du domaine 2D et de la grille CUDA

Nous n'allons pas passer de grille dont les dimensions ne sont pas compatibles avec les dimensions du domaine.
Même si cela est techniquement possible à réaliser, nous ne vous demandons pas de gérer ces cas.

Nous ne testerons que des dimensions cohérentes, et chaque direction de la grille sera au minumum aussi grande que la direction équivalente du domaine. 
C'est-à-dire que `gridDim.X * blockDim.x >= M` et `gridDim.y * blockDim.y >= N`.
Nous n'allons pas passer une grille plus petite que le domaine, ceci imposerait à chaque thread de calculer plusieurs points de grille.

Pour résumer, vous pouvez supposer que l'utilisateur de votre API sera averti et raisonnable.

## Taille maximum de la grille CUDA

Nous n'allons pas dépasser la taille de grille maximum, ceci imposerait aussi à chaque thread de calculer plusieurs points de grille.
En jetant un oeil à la [documentation CUDA](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#features-and-technical-specifications__technical-specifications-per-compute-capability) et comme cela a été présenté lors de l'une des séance GPU, on peut voire qu'avec la contrainte précédente d'une grille compatible, il n'est pas possible de générer un tel domaine.

En effet, un tel domaine occuperait trop de mémoire et ne serait allouable sur aucun des GPUs à disposition sur Ygdrasil ou Baobab.
Le plus gros GPU en terme de mémoire est un A100 [Ampere](https://en.wikipedia.org/wiki/Ampere_(microarchitecture)) et ne contient que 40 GB.
Ceci correspondrait à 5'000'000'000 `floats` et donc autant de point de grilles. 
Le tout restant inférieur au 2'147'483'647 blocks, avec ses 1024 threads par block.

Avec du single GPU et sans mémoire unifiée, la limite est simplement technique.
