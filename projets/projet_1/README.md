# Projet 1: l'équation de la chaleur

Le but de ce projet **noté** est de procéder à une implémentation simple d'un [code stencil](https://en.wikipedia.org/wiki/Stencil_code). 
Ceci dans le but de résoudre l'équation de la chaleur sur un domaine carré en deux dimensions. 
L'implémentation parallèle se fera avec MPI en C afin que vous puissiez exécuter votre code sur Baobab.

L'algorithme décrit dans cet énoncé est admis sans démonstration.
Il n'est donc pas nécessaire de comprendre les mathématiques associées à la résolution de l'équation de la chaleur, ni de comprendre l'équation elle-même.
En effet, il faudrait une ou deux leçons complètes (c-à-d de deux à quatre heure) pour en introduire tous les conceptes.
Néanmoins, la "traduction" de l'approche en algorithme devra être claire pour l'étudiant et son fonctionnement devrait être intuitif.

Toutefois, les étudiants curieux peuvent se référer à: [équation de la chaleur](https://en.wikipedia.org/wiki/Heat_equation), ainsi qu'aux concepts suivants pour comprendre la description du problème:
- description du [flux](https://en.wikipedia.org/wiki/Flux),
- le [théorème de la divergence](https://en.wikipedia.org/wiki/Divergence_theorem),
- et l'[opérateur de Laplace](https://en.wikipedia.org/wiki/Laplace_operator),
- ou encore l'[équation de Laplace](https://en.wikipedia.org/wiki/Laplace%27s_equation).

Ensuite pour la résolution numérique du problème: 
- la [série de Taylor](https://en.wikipedia.org/wiki/Taylor_series),
- les [différences fines](https://en.wikipedia.org/wiki/Finite_difference).

Les articles sont proposés en anglais, car je trouve que les articles Wikipedia anglophones couvrant les mathématiques sont plus claires que ceux écrits en français. 
Les articles francophones étant souvent très formels sans tenter de donner une intuition sur ce qui est expliqué.
Il s'agit d'un avis personel, libre à vous de lire les articles dans la langue de votre choix.

## Diffusion de la chaleur sur un plaque rectangulaire

Le problème que nous cherchons à résoudre est le suivant:
- on a une plaque rectangulaire,
- on ne s'intéresse pas à la température initiale de la plaque,
- on applique une source de chaleur constante sur les bords de la plaque,
- la chaleur va se propager (diffuser) vers/depuis les bords de la plaque (du chaud vers le froid, principe de la thermodynamique),
- au bout d'un certain temps, la plaque va atteindre un état stable (c-à-d. plus de propagation, plus de changement de température au cours du temps).

Ce phénomène se décrit bien mathématiquement par une équation aux dérivées partielles dont une solution analytique a été proposée par [Jean-Baptiste Joseph Fourier](https://en.wikipedia.org/wiki/Joseph_Fourier).
Mais dans notre cas, nous n'allons pas chercher à résoudre analytiquement l'équation de la chaleur, mais nous allons simplement appliquer un schéma numérique (un algorithme) pour approximer la solution.

Tout d'abord, on ne peut pas considérer un domaine (c-à-d. notre plaque) continu. 
Nous somme obligé de considérer un domaine discret, dénoté par $`U`$.
Ceci est inhérent à l'utilisation d'un ordinateur pour représenter le domaine.

Le domaine s'apparente donc à une grille et nous sommes intéressés uniquement à approximer les valeurs de chaleur (la température) aux coordonnées $`(i,j)`$ rouges de la grille $`U`$:

![dom](fig/domain.png)

Les points aux coordonnées bleues sont les valeurs aux bords et ce sont ces points qui restent constante au court du temps. 
Il n'est donc pas nécessaire de les "résoudre", vu qu'on les connait (ils sont constants). 
Néanmoins ce sont eux qui vont définir la répartition finale de la température sur la plaque.

Il est important de noter que les points de grilles (dénotés $`u_{ij}`$) sont équidistants les uns des autres.
Ceci même si la figure précédente ne l'illustre pas parfaitement.
Ce qui implique qu'on peut facilement représenter ce domaine à l'aide d'un tableau en deux dimensions composé de valeurs réelles.

Une fois le domaine discrétisé et les valeurs aux bords imposées, la résolution se fait à l'aide d'une simple moyenne.
En effet, une solution au problème consiste à calculer l'évolution de la température d'un point $`u_{ij}`$ au temps $`t`$, comme étant la température moyenne de ses voisins au temps $`t-1`$.
Ce calcul s'applique du temps initial, noté $`t_0`$, jusqu'au temps où le système converge, noté $`t_n`$.
C'est-à-dire que les températures des tous les points $`u_{ij}`$ du domaine ne changent plus en appliquant la moyenne.

Faire une telle opération se nomme une application de stencil.
Un stencil est un pattern ou motif fix qui décrit quels voisins sont utilisés pour calculer la valeur d'un point $`u_{ij}`$.
Il est appliqué en même temps à tous les points de l'espace considéré (application synchrone). 

Le stencil qui nous intéresse peut être schématisé comme:

![sten](fig/stencil.png)

donc dans notre cas nous utiliseront les quatre voisins (points blancs) pour calculer la valeur d'un point du domaine (point noir, notre $`u_{ij}`).

**Erreur dans le dessin: il y a quatre voisins blancs: nord, sud, est et ouest. Le point de grille blanc au sud-est est une erreur et devrait être rouge**.

Plus formellement, nous calculons la moyenne des quatre voisins à chaque application du stencil au cours du temps et pour chaque point du domaine:

$` u_{i,j}^{t+1} = \frac{1}{4} \cdot (u_{i-1,j}^{t} + u_{i+1,j}^{t} + u_{i,j-1}^{t} + u_{i,j+1}^{t} )`$

où $`u_{i,j}^{t}`$ est la valeur (la chaleur) à la coordonnée $`(i,j)`$ du domaine $`U`$ à l'itération $`t`$.

Note pour les étudiants ayant suivi le cours de VISNUM: 
- vous avez déjà appliqué un stencil, p.ex. avec le filtre moyenneur (passe-bas),
- il s'agit du **même** filtre que celui appliqué pour détecter les contours d'un objet. D'ailleur ce fitre s'appelait le filtre de Laplace.

Pour atteindre l'état stationnaire, on applique itérativement le stencil sur tous les points $`(i,j)`$ du domaine et on produit successivement les domaines: $`U^0`$ (état inital, au temps $`t_0`$), $`U^1, U^2, \ldots, U^n`$ (au temps $`t_n`$).
Ceci jusqu'à ce que les valeurs convergent. 
Numériquement cela implique que toutes les points points $`(i,j)`$ de $`U`$ ne changent plus suffisamment d'une itération à l'autre.

Mais pour des questions de confort, spécialement pour les mesures de performance du code, nous utiliserons un nombre d'itération maximum $`n`$ à appliquer.

### Pseudo-code séquentiel des boucles temporelle et spatiale

Le pseudo-code suivant illustre l'algorithme approximant la solution de l'équation de Laplace pour un nombre d'itérations fixe `max_T`, pour un domaine discret composé de `max_I` $`\times`$ `max_J` points:

```c
U = init()
for (int t = 0; t < max_T; t++) 
{
    for (int i = 0; i < max_I; i++)
    {
        for (int j = 0; j < max_J; j++)
        {
            U_temp[i][j] = 0.25 * (
                U[i-1][j] + U[i+1][j] + 
                U[i][j-1] + U[i][j+1]
            )
        }
    } 
    U = U_temp
}
```

Pour l'état initiale (i.e. `init()`) vous pouvez choisir de mettre à 0 les bords du bas et de gauche, et à 1 les bords du haut et de droite.
La valeur à l'intérieur du domaine peut être de 0, mais ces valeurs importent peu:

```c
U = 0
for (int i = 0; i < max_I; i++)
{
    U[i][0] = 0.0 // première colonne
    U[i][max_J - 1] = 1.0 // dernière colonne
}

for (int j = 0; i < max_J; j++)
{
    U[0][j] = 1.0 // première rangée
    U[max_i - 1][j] = 0.0 // dernière rangée
}
```

Votre code doit néanmoins fonctionner avec des conditions aux bords arbitraires.

Nous rappelons que les valeurs au bord du domaine ne changent pas au cours du temps.

Voici un exemple de taille $`7 \times 7`$ d'un état initial $`U^0`$:

```
1.0 1.0 1.0 1.0 1.0 1.0 1.0
0.0 0.0 0.0 0.0 0.0 0.0 1.0
0.0 0.0 0.0 0.0 0.0 0.0 1.0
0.0 0.0 0.0 0.0 0.0 0.0 1.0
0.0 0.0 0.0 0.0 0.0 0.0 1.0
0.0 0.0 0.0 0.0 0.0 0.0 1.0
0.0 0.0 0.0 0.0 0.0 0.0 0.0
```

et après application d'une itération nous obtenons $`U^1`$:

```
1.0  1.0  1.0  1.0  1.0  1.0  1.0
0.0  0.25 0.25 0.25 0.25 0.5  1.0
0.0  0.0  0.0  0.0  0.0  0.25 1.0
0.0  0.0  0.0  0.0  0.0  0.25 1.0
0.0  0.0  0.0  0.0  0.0  0.25 1.0
0.0  0.0  0.0  0.0  0.0  0.25 1.0
0.0  0.0  0.0  0.0  0.0  0.0  0.0
```

## Parallélisation du code

Pour paralléliser nous allons simplement découper le domaine en bande (horizontales ou verticales, à choix) et attribué le calcul de chaque bande à un processus MPI. 
Vous aurez donc autant de rang ou tâches MPI qu'il y aura de bande.

Evidemment le nombre de bande doit correspondre aux ressources demandées sur la machine parallèle et les bandes doivent être de tailles égales dans la mesure du possible (selon si le nombre de tâches divise le nombre de points sur l'axe $`i`$ ou $`j`$).

Par exemple, avec trois tâches MPI à disposition (et donc autant de ressource de calcul), voici un exemple de découpage:

![pardom](fig/paradom.png)

Notez que c'est à vous de réfléchir comment adapter cette approche à la résolution de l'équation de la chaleur.

## Travail à réaliser

Vos travaux seront rendu sur un dépôt Git déjà existant à votre nom dans le groupe suivant:
https://githepia.hesge.ch/hpc_2122_projet_1

C'est-à-dir si votre mail est `berk.prout@etu.hesge.ch`, votre projet assigné sera `berk.prout`.

Si vous ne voyez pas votre projet, ou que vous n'avez pas les droits d'écritures, signalez-le au plus vite.
N'attendez pas la dernière minute pour tester vos accès, faites ceci dès que vous aurez terminé la lecture de cet énoncé.

Vérifiez que les personnes ayant accès à votre dépôt en plus de vous même sont :

- Christophe Charpilloz (christophe.charpilloz@hesge.ch)
- Pierre Künzli (pierre.kunzli@hesge.ch)
- Michaël El Kharroubi (michael.el-kharroubi@hesge.ch)

Vous pouvez vérifier cela via `project information -> members` dans le menu de gauche de l'interface GitLab.

Les excuses de type: "je suis en retard car je n'ai pas pu pusher mon travail" ne seront pas acceptées à moins que vous ayez signaler votre problème assez tôt.
C'est-à-dire dans les 24/48h qui suivent la distribution de cet énoncé.

L'entièreté du travail, partie implémentation et mesure de performance doivent être rendue au plus tard le 
**29 NOVEMBRE 2021**.

### Partie implémentation

Vous devez implémenter la résolution de l'équation de Laplace en parallèle avec MPI en C.
Votre code doit pouvoir se **compiler** et s'exécuter sur la machine **Baobab**.
Sans quoi votre travail ne sera pas évalué et sanctionné par la note minimale.

Votre exécutable devra être nommé `laplace.out` et prendre à la ligne de commandes deux variantes possibles.

La première variante, les paramètres sont les suivants (passage de trois paramètres):
- nombre de lignes du domaine (dénoté ici par `N`)
- nombre de colonnes du domaine (dénoté ici par `M`)
- nombre d'itérations à appliquer

et on considère les conditions aux bords suivantes:
- haut = 1.0
- bas = 0.0
- gauche = 0.0
- droite = 1.0

Par exemple, la commande:
```
$ laplace.out 10 12 300
```
produira un domaine composé de 120 points de grille sur lesquels on appliquera 300 itérations du stencil introduit précédemment avec les conditions aux bords décrites ci-dessus.

Après 300 itérations, la solution ressemblera à:

![dom](fig/mini_heat.png)

La deuxième variante, les paramètres sont les suivants (passage de deux paramètres):
- un fichier texte contenant les conditions aux bords avec une ligne par condition, dans l'ordre suivant haut, bas, gauche, droite. 
Chaque ligne contient des valeurs numériques qui sont séparées par un espace.
- nombre d'itérations à appliquer

Évidemment, le nombres de valeurs par ligne doit correspondre entre le haut et le bas, ainsi qu'entre la gauche et la droite.
On déduit également facilement la taille du domaine en fonction de la dimension des bords.

Par exemple, la commande:
```
$ laplace.out borders.txt 250
```
où `borders.txt` vaut:
```
0.2 0.3 0.4 0.5 0.6 0.7
0.8 0.9 1.0 1.1 1.2 1.3
0.1 0.1 0.1
0.2 0.2 0.2
```

produirait un domaine:
```
0.2 0.3 0.4 0.5 0.6 0.7
0.1 0.0 0.0 0.0 0.0 0.2 
0.1 0.0 0.0 0.0 0.0 0.2
0.1 0.0 0.0 0.0 0.0 0.2
0.8 0.9 1.0 1.1 1.2 1.3
```
et donc produit un domaine de taille `6 x 5`, donc 30 points de grille, bords compris.

(Merci à l'étudiant `STUDENT_NAME_HERE` pour sa remarque/correction concernant la dimension du domaine)

Le nombre de bandes devra être déduit automatiquement par le nombre de tâches MPI demandées.
L'exécution doit produire un fichier text nommé `laplace.txt` à la racine du projet qui contient le domaine (càd. le tableau) à la fin des itérations.
Avec une ligne par rangée du tableau, où chaque valeur est séparée par un espace. 

Lors de l'évaluation de votre travail, les modules chargés seront ceux du module foss:
```
$ module load foss
```
qui correspond au module par défaut `foss/2021a` et charge les dépendances suivantes:
```
  1) GCCcore/10.3.0    5) numactl/2.0.14      9) hwloc/2.4.1      13) libfabric/1.12.1  17) FlexiBLAS/3.0.4
  2) zlib/1.2.11       6) XZ/5.2.5           10) OpenSSL/1.1      14) PMIx/3.2.3        18) FFTW/3.3.9
  3) binutils/2.36.1   7) libxml2/2.9.10     11) libevent/2.1.12  15) OpenMPI/4.1.1     19) ScaLAPACK/2.1.0-fb
  4) GCC/10.3.0        8) libpciaccess/0.16  12) UCX/1.10.0       16) OpenBLAS/0.3.15   20) foss/2021a
```

Vous devez aussi fournir un `Makefile` contenant les cibles suivantes:
- `clean`: qui efface tous les fichiers produit par l'operation de compilation
- `build`: qui compile votre application et produit l'exécutable `laplace.out` à la racine du projet
- `all`: qui applique un clean build (càd un clean suivit d'un build)

Vous pouvez considérer que les modules seront chargés avant l'appel à `make`.

Vous déposerez dans le projet que vous aurez créé (cf. instructions précédente):
- votre code source, càd tous les fichiers nécessaires à la compilation,
- votre `Makefile`.

Votre code sera évalué sur :
- compilation (simple à comprendre)
- exécution, p. ex. est-ce qu'il fonctionne toujours selon les différents paramètres choisis,
- sa performance.

Pour la performance, vous ne devez pas forcément produire un code qui produit un speedup idéal, mais il ne faudrait pas non plus avoir un [code en C qui s'exécute aussi lentement que du Python](https://benchmarksgame-team.pages.debian.net/benchmarksgame/fastest/python3-gcc.html).

Avant la partie suivante, pensez à tester votre code:
- sur un domaine petit, et que quelques itéreations, de manière à ce que l'exécution soit rapide,
- comparez les résultats avec une exécution séquentielle, le code est relativement trivial à écrire (attention si vous [comparez des nombres en virgules flottante](https://stackoverflow.com/questions/4915462/how-should-i-do-floating-point-comparison)),
- testez votre code sur un noeud, et plusieurs noeuds sans nécessairement utiliser les noeuds en entier,
- il est possible que pour des tests multi-noeuds, `shared-cpu` soit plus disponible que `debug-cpu`, en particulier si tous les étudiants font leur tests en même temps,
- tester aussi votre code avec un nombre de tâches qui ne divise pas le nombre de lignes (ou colones) de votre domaine discret.

### Partie mesure de performance

Vous devez également faire des mesures de performance de votre code et produire une courbe de speedup de votre application parallèle.
Ceci afin d'observer si vous être bien en mesure d'accélérer éfficacement votre code sur un superordinateur.

**ATTENTION: lors de vos mesures, n'écrivez PAS le résultat dans le fichier `laplace.txt` !!** Désactivez cette partie, il ne fait pas de sens de mesurer cet I/O dans vos benchmarks.

Si votre code est bien testé, soumettez vos mesures en batch. 
La machine travaille pour vous. 
Donnez assez de travail pour que l'exécution la plus rapide soit quand même de l'ordre de la minute et pas trop de manière à ce que l'exécution séquentielle ne prennent pas plus de 60 minutes.
Vous avez deux axes sur lesquels vous pouvez jouer pour ajouter du travail, mais ils peuvent influencer les performances de manière différentes.

Il y a potentiellement de la place sur la machine, vos mesures, càd. jobs, peuvent s'exécuter en même temps.
Si vous arrivez à tout soumettre et exécuter en même temps (peu probable), le temps pris par vos mesures est celui d'une exécution séquentielle.
Pensez bien à la forme de vos outputs de manière à pouvoir les parser facilement lorsque toutes vos mesures sont terminées.

Vous présenterez tous vos résultats sous forme d'un rapport. 
Vous décrirez également la stratégie employée pour paralléliser votre code.
Ce rapport peut être rendu:
- au format PDF nommé avec votre préfix de mail HEPIA, par ex. `berk.prout.pdf`
- ou bien au format Markdown `rapport.md`.

Votre rapport sera évalué sur :
- la description de votre stratégie,
- la pertinance des mesures faites,
- la clareté de la présentation de ces mesures,
- la discussion, si nécessaire, pour expliquer les (mauvaises) performances de votre code. Si vous n'êtes pas dans un cas idéal de speedup (ce qui sera probablement le cas).

**Le dernier commit pris en compte pour la correction du projet, sera le dernier commit fait le 29 novembre 2021.**
