## Enseignants

Christophe Charpilloz - christophe.charpilloz@hesge.ch

Pierre Künzli - pierre.kunzli@hesge.ch

Assistant : Michaël El Kharroubi michael.el-kharroubi@hesge.ch 

## Horaires

TP jour : lundi 15h-16h45

Théorie jour + soir : lundi 17h-18h30

TP soir : lundi 18h30-20h

## Ressources

La documentation de Baobab et Yggdrasil : https://doc.eresearch.unige.ch/hpc/start

Un bon site sur MPI : https://mpitutorial.com/

La documentation de SLURM : https://slurm.schedmd.com/

La documentation de Open MPI : https://www.open-mpi.org/doc/current/

La documentation de CUDA: https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html
